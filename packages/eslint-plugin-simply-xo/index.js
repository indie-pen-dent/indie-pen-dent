/* eslint-disable unicorn/prefer-module */
const {name, version} = require('./package.json');
const javascript = require('./javascript.js');
const typescript = require('./typescript.js');

module.exports = {
	meta: {
		name,
		version,
	},
	rules: {},
	configs: {
		javascript,
		typescript: {
			...typescript,
			extends: [
				...javascript.extends,
			],
			settings: {
				...javascript.settings,
				...typescript.settings,
			},
			plugins: [
				...javascript.plugins,
				...typescript.plugins,
			],
			rules: {
				...javascript.rules,
				...typescript.rules,
			},
		},
	},
};
