import type { Http } from './type';

interface Success {
    success: true;
    path: string;
}

export class ImageService {
    #http: Http;

    constructor(http: Http) {
        this.#http = http;
    }

    uploadImage(image: File) {
        const formData = new FormData();
        formData.append('image', image, image.name);

        return this.#http.post<Success>('/api/upload', formData);
    }
}
