import type { Http } from './type';

interface UserData {
    gitService: string;
    settings: Record<string, unknown>
}

interface Success {
    success: true;
}

export class UserService {
    #http: Http;

    constructor(http: Http) {
        this.#http = http;
    }

    get(): Promise<UserData> {
        return this.#http.get<Omit<UserData, 'apiKey'>>('/api/user');
    }

    create(data: UserData): Promise<Success> {
        return this.#http.post<Success>('/api/user', data);
    }

    update(data: UserData): Promise<Success> {
        return this.#http.put<Success>('/api/user', data);
    }
}
