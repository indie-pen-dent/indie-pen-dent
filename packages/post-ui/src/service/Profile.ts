import type { Http } from './type';

interface Profile {
    name: string;
    photoUrl: string;
    website: string;
    bio: string;
}

export class ProfileService {
    #http: Http;

    constructor(http: Http) {
        this.#http = http;
    }

    get(): Promise<Profile> {
        return this.#http.get<Profile>('/api/profile');
    }
}
