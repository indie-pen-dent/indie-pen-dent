import type { Http } from './type';
import { FetchAdapter } from './http.js';
import { UserService } from './User.js';
import { ImageService } from './Images.js';
import { ProfileService } from './Profile.js';
import { PostService } from './Post.js';
import { AuthService } from './Auth';
import { baseUrl } from '../config.js';

const http = new FetchAdapter(baseUrl);

class Service {
    #http: Http;

    public user: UserService;
    public image: ImageService;
    public profile: ProfileService;
    public post: PostService;
    public auth: AuthService;

    constructor(http: Http) {
        this.#http = http;

        this.user = new UserService(this.#http);
        this.image = new ImageService(this.#http);
        this.profile = new ProfileService(this.#http);
        this.post = new PostService(this.#http);
        this.auth = new AuthService(this.#http);
    }

    setAuthToken(token: string) {
        this.#http.token = token;
    }

    clearAuthToken() {
        this.#http.token = null;
    }
}

export const service = new Service(http);
