import type { Http } from './type';

export class FetchAdapter implements Http {
    private baseUrl: string;
    public token: string | null = null;

    constructor(baseUrl: string) {
        this.baseUrl = baseUrl;
    }

    private async request<T>(url: string, method: string, body?: any): Promise<T> {
        const headers: Record<string, string> = {};

        if (!(body instanceof FormData)) {
            headers['Content-Type'] = 'application/json';
        }

        if (this.token) {
            headers['Authorization'] = `bearer ${this.token}`;
        }

        const formattedBody = (body instanceof FormData)
            ? body
            : JSON.stringify(body);

        const response = await fetch(url, {
            method,
            credentials: this.token ? 'omit' : 'include',
            headers,
            body: formattedBody,
        });

        if (!response.ok) {
            throw new Error(`Request failed with status ${response.status}`);
        }

        return response.json() as Promise<T>;
    }

    public async get<T>(path: string): Promise<T> {
        const url = `${this.baseUrl}${path}`;
        return this.request<T>(url, 'GET');
    }

    public async post<T>(path: string, body: any): Promise<T> {
        const url = `${this.baseUrl}${path}`;
        return this.request<T>(url, 'POST', body);
    }

    public async put<T>(path: string, body: any): Promise<T> {
        const url = `${this.baseUrl}${path}`;
        return this.request<T>(url, 'PUT', body);
    }

    public async delete<T>(path: string): Promise<T> {
        const url = `${this.baseUrl}${path}`;
        return this.request<T>(url, 'DELETE');
    }
}
