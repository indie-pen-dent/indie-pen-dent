import type { Http } from './type';

interface Success {
    success: true;
    path: string;
}

interface Post {
    title?: string;
    content: string;
}

export class PostService {
    #http: Http;

    constructor(http: Http) {
        this.#http = http;
    }

    async uploadPost(post: Post) {
        return await this.#http.post<Success>('/api/post', post);
    }

    async getPosts() {
        return await this.#http.get<unknown>('/api/post');
    }

    async getAPost(name: string) {
        return await this.#http.get<unknown>(`/api/post/${name}`);
    }
}
