import type { Http } from './type';

interface Success {
    token: string;
    me: string;
}

export class AuthService {
    #http: Http;

    constructor(http: Http) {
        this.#http = http;
    }

    getToken() {
        return this.#http.get<Success>('/api/token');
    }
}
