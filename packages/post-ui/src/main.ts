import { createApp } from 'vue'
import * as Sentry from '@sentry/vue';

import { pinia } from './stores/index.js'
import App from './App.vue'
import router from './router'

import 'vuetify/styles';
import '@mdi/font/css/materialdesignicons.css'

import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';
import { commitSha } from './config';

const vuetify = createVuetify({
    components,
    directives
})

const app = createApp(App);

app.use(vuetify);
app.use(pinia);
app.use(router);
app.mount('#app');

if (!import.meta.env.Dev) {
    Sentry.init({
        release: commitSha,
        environment: import.meta.env.DEV ? "DEV" : "PROD",
        app,
        dsn: "https://e68108904507408eafd8db0f4a4c9515@o280795.ingest.sentry.io/4505426803752960",
        integrations: [
            new Sentry.BrowserTracing({
                // Set `tracePropagationTargets` to control for which URLs distributed tracing should be enabled
                tracePropagationTargets: ["localhost", /^https:\/\/yourserver\.io\/api/],
                routingInstrumentation: Sentry.vueRouterInstrumentation(router),
            }),
            new Sentry.Replay(),
        ],
        // Performance Monitoring
        tracesSampleRate: 0.75, // Capture 100% of the transactions, reduce in production!
        // Session Replay
        replaysSessionSampleRate: 0.1, // This sets the sample rate at 10%. You may want to change it to 100% while in development and then sample at a lower rate in production.
        replaysOnErrorSampleRate: 0.75, // If you're not already sampling the entire session, change the sample rate to 100% when sampling sessions where errors occur.
    });
}
