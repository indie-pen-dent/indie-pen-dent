export interface PersistenceStore<T> {
    persist: (dataName: string, key: string, data: T) => void;
    retrieve: (dataName: string, key: string) => T | null;
}
