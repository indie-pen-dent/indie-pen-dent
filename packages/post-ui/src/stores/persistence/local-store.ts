import type { PersistenceStore } from '../type';

export class LocalStore<T> implements PersistenceStore<T> {
    persist(dataName: string, key: string, data: T): void {
        localStorage.setItem(`${dataName}${key}`, JSON.stringify(data));
    }

    retrieve(dataName: string, key: string): T | null {
        const itemIndex = this.#createKey(dataName, key);

        const rawData = localStorage.getItem(itemIndex);

        return rawData ? JSON.parse(rawData) : null;
    }
    
    #createKey(dataName: string, key: string) {
        return `${dataName}${key}`;
    }
}
