import { ref } from 'vue';
import type { Ref } from 'vue';
import { defineStore } from 'pinia';

import { useBlogStore } from './blogs';
import { persistence } from './persistence/main.js';

interface State {
    bio?: string;
    name?: string;
    website?: string;
    photoUrl?: string;
    fetched: boolean;
}

const PERSITED_NAME = 'profileData';

export const useProfileStore = defineStore('user', () => {
    const bio: Ref<string | undefined> = ref(undefined);
    const name: Ref<string | undefined> = ref(undefined);
    const website: Ref<string | undefined> = ref(undefined);
    const photoUrl: Ref<string | undefined> = ref(undefined);
    const fetched: Ref<boolean> = ref(false);

    const blogStore = useBlogStore();

    function loadData() {
        const activeBlog = blogStore.activeBlog;

        if (typeof activeBlog !== 'string') {
            bio.value = undefined;
            name.value = undefined;
            website.value = undefined;
            photoUrl.value = undefined;
            fetched.value = false;
            return;
        }

        const persistedData = persistence.retrieve(
            PERSITED_NAME,
            activeBlog
        ) as Partial<State>;

        bio.value = persistedData?.bio;
        name .value= persistedData?.name;
        website.value = persistedData?.website;
        photoUrl.value = persistedData?.photoUrl;
        fetched.value = persistedData?.fetched ?? false;

    }

    loadData();
    blogStore.subscribeToBlogChange(loadData);

    function setProfile(profile: Omit<State, 'fetched' | 'loading'>) {
        bio.value = profile.bio;
        name.value = profile.name;
        website.value = profile.website;
        photoUrl.value = profile.photoUrl;

        fetched.value = true;

        persistData();
    }

    function persistData() {
        const blogStore = useBlogStore();

        const activeBlog = blogStore.activeBlog;

        if (typeof activeBlog !== 'string') {
            return;
        }

        return persistence.persist(PERSITED_NAME, activeBlog, {
            bio: bio.value,
            name: name.value,
            website: website.value,
            photoUrl: photoUrl.value,
            fetched: fetched.value,
        });
    }

    return {
        bio,
        name,
        website,
        photoUrl,
        fetched,
        setProfile,
    };
})
