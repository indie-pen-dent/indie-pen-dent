import { ref } from 'vue';
import { defineStore } from 'pinia';

import { useBlogStore } from './blogs';
import { persistence } from './persistence/main.js';

interface State {
    isAuthed: boolean;
}

const PERSITED_NAME = 'authData';

export const useAuthStore = defineStore('auth',() => {
    const isAuthed = ref(false);
    const blogStore = useBlogStore();

    function loadData() {

        const activeBlog = blogStore.activeBlog;

        if (typeof activeBlog !== 'string') {
            isAuthed.value = false;
            return;
        }

        const persistedData = persistence.retrieve(PERSITED_NAME, activeBlog) as Partial<State>;

        isAuthed.value = persistedData?.isAuthed ?? false;
    }

    loadData();
    blogStore.subscribeToBlogChange(loadData);

    function setAuthed() {
        isAuthed.value = true;

        persistData();
    }

    function persistData() {
        const blogStore = useBlogStore();

        const activeBlog = blogStore.activeBlog;

        if (typeof activeBlog !== 'string') {
            return;
        }

        persistence.persist(
            PERSITED_NAME,
            activeBlog,
            { isAuthed: isAuthed.value }
        );
    }

    return {
        setAuthed,
        isAuthed
    };
});
