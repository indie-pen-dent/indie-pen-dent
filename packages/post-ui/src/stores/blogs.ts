import { reactive } from 'vue';
import { defineStore } from 'pinia';

import { service } from '../service/main.js';
import { persistence } from './persistence/main.js';

interface State {
    activeBlog: string | null;
    blogAccounts: Record<string, string>;
}

const PERSISTED_NAME = 'blogs';

function loadData(): State {
    const persistedData = persistence.retrieve(PERSISTED_NAME, '') as Partial<State>;

    const initialState: State = {
        activeBlog: persistedData?.activeBlog ?? null,
        blogAccounts: persistedData?.blogAccounts ?? {}
    };


    if (initialState.activeBlog) {
        const blogToken = initialState.blogAccounts[initialState.activeBlog];
        service.setAuthToken(blogToken);
    }

    return initialState;
}

export const useBlogStore = defineStore('blog', {
    state: (): State => reactive(loadData()),
    actions: {
        setActiveBlog(blog: string | null) {
            this.activeBlog = blog;

            if (blog) {
                service.setAuthToken(this.blogAccounts[blog]);
            } else {
                service.clearAuthToken();
            }

            this.persistData();
        },
        subscribeToBlogChange(callback: () => void) {
            this.$onAction(({ name, after}) => {
                if (name !== 'setActiveBlog') {
                    return;
                }

                after(callback);
            });
        },
        addBlog(blog: string, token: string) {
            if (this.blogAccounts[blog]) {
                return;
            }

            this.blogAccounts[blog] = token;

            this.persistData();
        },
        persistData() {
            persistence.persist(PERSISTED_NAME, '', {
                activeBlog: this.activeBlog,
                blogAccounts: this.blogAccounts,
            });
        }
    }
});
