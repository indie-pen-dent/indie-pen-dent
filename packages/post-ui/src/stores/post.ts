import { reactive, ref } from 'vue';
import { defineStore } from 'pinia';

import { useBlogStore } from './blogs';
import { persistence } from './persistence/main.js';

interface ImageData {
    path: string;
    ready: boolean;
}

interface State {
    title: string;
    content: string;
    images: ImageData[];
    posts: unknown[];
}

const PERSITED_NAME = 'postData';


export const usePostStore = defineStore('post', () => {
    const blogStore = useBlogStore();

    const title = ref('');
    const content = ref('');
    const images: ImageData[] = reactive([]);
    const posts = ref();

    function loadData() {

        const activeBlog = blogStore.activeBlog;

        if (typeof activeBlog !== 'string') {
            title.value = '';
            content.value = '';
            setPhotos([]);
            return;
        }

        const persistedData = persistence.retrieve(
            PERSITED_NAME,
            activeBlog
        ) as Partial<State>;

        if (!persistedData) {
            return;
        }

        title.value = persistedData?.title ?? '';
        content.value = persistedData?.content ?? '';

        setPhotos(persistedData?.images ?? []);

        return;
    }

    loadData();
    blogStore.subscribeToBlogChange(loadData);

    function setTitle(inputTitle: string) {
        title.value = inputTitle;

        persistData();
    }

    function setContent(inputContent: string) {
        content.value = inputContent;

        persistData();
    }

    function addPhotos(inputImages: ImageData[]) {
        setPhotos(inputImages);

        persistData();
    }

    function removePhoto(index: number) {
        images.splice(index, 1);
    }

    function setPhotos(inputImages: ImageData[]) {
        images.length = 0;
        images.push(...(inputImages.map(reactive)));

        persistData();
    }

    function persistData() {
        const blogStore = useBlogStore();

        const activeBlog = blogStore.activeBlog;

        if (typeof activeBlog !== 'string') {
            return;
        }

        return persistence.persist(PERSITED_NAME, activeBlog, {
            title: title.value,
            content: content.value,
            images: images,
        });
    }

    function setPosts(postsFromServer: unknown) {
        posts.value = postsFromServer;
    }

    return {
        title,
        content,
        images,
        posts,
        setTitle,
        setContent,
        addPhotos,
        removePhoto,
        setPosts,
    };
})
