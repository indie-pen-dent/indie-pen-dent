import { createRouter, createWebHistory } from 'vue-router'
import { useAuthStore } from '../stores/auth.js';

const AUTH_ROUTE_NAME = 'auth';
const USER_ROUTE_NAME = 'user';
const AUTH_PASSED_ROUTE_NAME = 'auth_passed';

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/auth',
            name: AUTH_ROUTE_NAME,
            component: () => import('../views/Auth.vue')
        },
        {
            path: '/auth_passed',
            name: AUTH_PASSED_ROUTE_NAME,
            component: () => import('../views/AuthPassed.vue'),
        },
        {
            path: '/user',
            name: 'user',
            component: () => import('../views/UserAccount.vue')
        },
        {
            path: '/post',
            name: 'post',
            component: () => import('../views/PostBlog.vue'),
            children: [
                {
                    path: 'view/:postName',
                    name: 'viewPost',
                    props: true,
                    component: () => import('../components/post/old-post.vue'),
                },
                {
                    path: 'new',
                    name: 'newPost',
                    props: true,
                    component: () => import('../components/post/new-post.vue'),
                },
                {
                    path: '',
                    name: 'allPosts',
                    props: true,
                    component: () => import('../components/post/all-post.vue'),
                }
            ]
        },
        {
            path: '/:pathMatch(.*)',
            component: async () => {
                const authStore = useAuthStore();

                const isAuthed = authStore.isAuthed;

                if (!isAuthed) {
                    return import('../views/Auth.vue');
                }

                return import('../views/UserAccount.vue');
            }
        }
    ]
});

router.beforeEach(async (to) => {
    if (/api/.test(to.path)) {
        return true;
    }

    if (to.name === AUTH_PASSED_ROUTE_NAME) {
        return true;
    }

    const authStore = useAuthStore();

    const isAuthed = authStore.isAuthed;

    const isAuthedAndNavigatingToLogin = to.name === AUTH_ROUTE_NAME && isAuthed;

    if (isAuthedAndNavigatingToLogin) {
        return { name: USER_ROUTE_NAME };
    }

    const isUnauthedAndNavigatingToNoneAuth = to.name !== AUTH_ROUTE_NAME && !isAuthed;

    if (isUnauthedAndNavigatingToNoneAuth) {
        return { name: AUTH_ROUTE_NAME };
    }

    return true;
});

export default router
