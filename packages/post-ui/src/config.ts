export const baseUrl = import.meta.env.DEV
    ? 'http://localhost:3000'
    : '';

export const commitSha = import.meta.env.VITE_COMMIT_SHA ?? 'DEV';
