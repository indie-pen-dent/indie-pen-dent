import { fileURLToPath, URL } from 'node:url'

import { VitePWA, VitePWAOptions } from 'vite-plugin-pwa'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { sentryVitePlugin } from "@sentry/vite-plugin";

const pwaOptions: Partial<VitePWAOptions> = {
    workbox: {
        navigateFallbackDenylist: [
            /^\/api\/authenticate/,
            /^\/api\/callback/
        ],
    },
    manifest: {
        name: 'Skribilo blogging app',
        short_name: 'Skribilo.blog',
        description: 'App for posting to blogs',
        icons: [
            {
                src: 'pwa-72x72.png',
                sizes: '72x72',
                type: 'image/png'
            },
            {
                src: 'pwa-192x192.png',
                sizes: '192x192',
                type: 'image/png'
            },
            {
                src: 'pwa-512x512.png',
                sizes: '512x512',
                type: 'image/png',
                purpose: 'any'  
            }
        ]
    }
};

// https://vitejs.dev/config/
export default defineConfig({
    base: 'app',
    build: {
        target: 'esnext',
    },
    plugins: [
        vue(),
        vueJsx(),
        VitePWA(pwaOptions),
        sentryVitePlugin({
            org: "steven-chetwynd",
            project: "skribilo-ui",
            // Auth tokens can be obtained from https://sentry.io/settings/account/api/auth-tokens/
            // and need `project:releases` and `org:read` scopes
            authToken: process.env.SENTRY_AUTH_TOKEN,
        }),
    ],
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    }
})
