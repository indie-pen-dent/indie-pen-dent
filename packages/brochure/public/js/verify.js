{
    const emailElement = document.getElementById('email');
    const blogNameElement = document.getElementById('blog-name');

    const email = sessionStorage.getItem('email');
    const blogName = sessionStorage.getItem('blogName');

    emailElement.value = email;
    blogNameElement.value = blogName;
}
