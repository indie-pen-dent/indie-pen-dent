(() => {
  
  const url = new URL(window.location.href);

  const errorString = url.searchParams.get('details');

  if (!errorString) {
    return;
  }

  const jsonErroString = atob(errorString);

  const error = JSON.parse(jsonErroString);

  const errorTitleElement = document.getElementById('error-message');

  const errorDetailsElement = document.getElementById('error-details');

  errorTitleElement.innerText = error.error;

  for (const detail of error.details) {
    const detailElement = document.createElement('li');
    
    detailElement.innerText = detail;
    
    errorDetailsElement.appendChild(detailElement);
  }
})();
