{
    const form = document.getElementById('initial-form');

    form.onsubmit = () => {
        const email = document.getElementById('initial-email').value;
        const blogName = document.getElementById('initial-blog-name').value;

        sessionStorage.setItem("email", email);
        sessionStorage.setItem("blogName", blogName);
    }
}
