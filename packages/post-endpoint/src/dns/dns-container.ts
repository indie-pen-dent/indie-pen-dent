import {dnsProvider, cloudflare} from '../config.js';
import {Cloudflare} from './clouldflare.js';
import {type Dns} from './types.js';

export class DnsContainer {
	getDns(): Dns {
		const dnsObject = this.#getDnsObject();

		const dnsProviderFactory = dnsObject[dnsProvider];

		if (!dnsProviderFactory) {
			throw new Error(`Unknown DNS Provider: "${dnsProvider}".`);
		}

		return dnsProviderFactory();
	}

	#getDnsObject(): Record<string, () => Dns> {
		return {
			cloudflare: () => new Cloudflare(cloudflare),
		};
	}
}
