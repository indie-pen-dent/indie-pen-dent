import fetch from 'node-fetch';
import {ThirdPartyError} from '../errors/third-party.js';
import type {Dns} from './types.js';

type CloudflareConfig = {
	apiToken: string;
	apiEmail: string;
	zoneId: string;
};

export class Cloudflare implements Dns {
	#apiUrl = 'https://api.cloudflare.com/client/v4';
	#config: CloudflareConfig;

	constructor(config: CloudflareConfig) {
		this.#config = config;
	}

	async create(hostname: string): Promise<string> {
		const url = this.#createUrl('');

		const body = {
			target: 'ipfs',
			name: `${hostname}.skribilo.blog`,
			description: hostname,
		};

		const result = await fetch(url, {
			method: 'POST',
			body: JSON.stringify(body),
			headers: this.#createHeaders(),
		});

		if (!result.ok) {
			const details = {
				name: 'cloudflare',
				url,
				status: result.status,
				responseBody: await result.text(),
			};
			throw new ThirdPartyError(
				'Error creating new DNS record.',
				details,
			);
		}

		const responseBody = (await result.json()) as {
			result?: {id?: string};
		};

		if (!responseBody.result?.id) {
			const details = {
				name: 'cloudflare',
				url,
				status: result.status,
				responseBody: await result.text(),
			};
			throw new ThirdPartyError(
				'Cloudflare did not return the host id',
				details,
			);
		}

		return responseBody.result.id;
	}

	async update(hostnameId: string, ipfsAddress: string): Promise<void> {
		const url = this.#createUrl(hostnameId);

		const body = {
			dnslink: ipfsAddress,
		};

		const result = await fetch(url, {
			method: 'PATCH',
			body: JSON.stringify(body),
			headers: this.#createHeaders(),
		});

		if (!result.ok) {
			const details = {
				name: 'cloudflare',
				url,
				status: result.status,
				responseBody: await result.text(),
			};
			throw new ThirdPartyError(
				'Cloudflare returned a none ok status',
				details,
			);
		}
	}

	#createUrl(hostnameId: string) {
		return `${this.#apiUrl}/zones/${
			this.#config.zoneId
		}/web3/hostnames/${hostnameId}`;
	}

	#createHeaders() {
		return {
			'X-Auth-Key': this.#config.apiToken,
			'X-Auth-Email': this.#config.apiEmail,
			'Content-Type': 'application/json',
		};
	}
}
