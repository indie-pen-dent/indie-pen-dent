export type Dns = {
	create: (hostname: string) => Promise<string>;
	update: (hostnameId: string, ipfsAddres: string) => Promise<void>;
};
