export type IntSchemaValidator<T> = {
	validate: (a: unknown) => a is T;
};

export type IntSchemaValidatorFactory = {
	build: <T>(schema: Record<string, unknown>) => IntSchemaValidator<T>;
};
