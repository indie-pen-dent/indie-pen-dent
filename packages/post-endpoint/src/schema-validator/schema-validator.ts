import Ajv, {type JSONSchemaType} from 'ajv';
import type {IntSchemaValidatorFactory} from './types.js';

export class SchemaValidatorFactory implements IntSchemaValidatorFactory {
	#ajv: Ajv | undefined;

	build<T>(schema: Record<string, unknown>) {
		const ajv = this.#getAjv();

		return {
			validate: ajv.compile(schema as JSONSchemaType<T>),
		};
	}

	#getAjv() {
		if (this.#ajv === undefined) {
			this.#ajv = new Ajv();
		}

		return this.#ajv;
	}
}
