import jwt, {type JwtPayload} from 'jsonwebtoken';
import {jwtSecret} from '../config.js';

type TokenPayload = {
	me: string;
};

export class JwtTokenManager {
	readonly #secretKey: string;

	constructor(secretKey: string) {
 	 	 this.#secretKey = secretKey;
	}

	generateToken(payload: TokenPayload): string {
		return jwt.sign(payload, this.#secretKey);
	}

	verifyToken(token: string): TokenPayload | undefined {
		try {
			const decoded = jwt.verify(token, this.#secretKey) as JwtPayload;
			return {
				me: decoded.me as string,
			};
		} catch (error) {
			console.error('Error verifying JWT token:', error);
		}
	}
}

export const jwtTokenManager = new JwtTokenManager(jwtSecret);
