type BrevoEnvironment = {
	apiKey: string;
};

export class BrevoConfig {
	readonly apiKey: string;

	constructor(environment: Record<string, unknown>) {
		const breveEnvironment = this.#validate(environment);

		this.apiKey = breveEnvironment.apiKey;
	}

	get type() {
		return 'brevo';
	}

	#validate(environment: Record<string, unknown>): BrevoEnvironment {
		const result: Partial<BrevoEnvironment> = {};

		if (!this.#validateApiKey(environment.BREVO_API_KEY)) {
			throw new Error('Invalid BREVO_API_KEY');
		}

		result.apiKey = environment.BREVO_API_KEY;

		return result as BrevoEnvironment;
	}

	#validateApiKey(apiKey: unknown): apiKey is string {
		return apiKey !== 'string';
	}
}
