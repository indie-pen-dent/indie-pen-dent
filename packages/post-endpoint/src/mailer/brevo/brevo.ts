import fetch from 'node-fetch';
import {ThirdPartyError} from '../../errors/third-party.js';
import type {SendMailParameters, Mailer} from '../types';

export class BrevoEmailSender implements Mailer {
	#apiKey: string;
	#url = 'https://api.brevo.com/v3/smtp/email';

	constructor(config: {apiKey: string}) {
		this.#apiKey = config.apiKey;
	}

	async sendEmail(data: SendMailParameters) {
		const headers = {
			'api-key': this.#apiKey,
		};

		const response = await fetch(this.#url, {
			method: 'POST',
			headers,
			body: JSON.stringify({
				sender: {
					name: data.from,
					email: data.from,
				},
				to: [
					{
						name: data.to,
						email: data.to,
					},
				],
				subject: data.subject,
				htmlContent: data.text,
			}),
		});

		if (!response.ok) {
			const details = {
				name: 'brevo',
				url: this.#url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Brevo: request failed with none ok status',
				details,
			);
		}
	}
}
