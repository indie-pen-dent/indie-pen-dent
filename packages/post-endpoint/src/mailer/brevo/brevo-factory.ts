import {BrevoEmailSender} from './brevo.js';
import {BrevoConfig} from './brevo-config.js';

export class BrevoFactory {
	create(environment: Record<string, unknown>) {
		const brevoConfig = new BrevoConfig(environment);

		return new BrevoEmailSender(brevoConfig);
	}
}
