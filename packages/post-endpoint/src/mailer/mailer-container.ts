import {MailgunFactory} from './mailgun/mailgun-factory.js';
import {BrevoFactory} from './brevo/brevo-factory.js';
import type {Mailer, MailerFactoryFunction} from './types';

export class MailerContainer {
	#environment;

	constructor(environment: Record<string, unknown>) {
		this.#environment = environment;
	}

	getMailer(): Mailer {
		const mailerObjects = this.#getMailersObject();

		const mailerType = this.#environment.MAILER;

		const mailerFactory
            = typeof mailerType === 'string'
            	? mailerObjects[mailerType]
            	: undefined;

		if (!mailerFactory) {
			throw new Error(`Unknown mailer type "${String(mailerType)}".`);
		}

		return mailerFactory();
	}

	#getMailersObject(): Record<string, MailerFactoryFunction> {
		return {
			mailgun: () => new MailgunFactory().create(this.#environment),
			brevo: () => new BrevoFactory().create(this.#environment),
		};
	}
}
