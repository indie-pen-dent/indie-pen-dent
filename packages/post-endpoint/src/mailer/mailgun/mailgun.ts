import fetch from 'node-fetch';
import {ThirdPartyError} from '../../errors/third-party.js';
import type {SendMailParameters, Mailer} from '../types';

export class MailgunEmailSender implements Mailer {
	private readonly apiKey: string;
	private readonly domain: string;
	private readonly region: string;
	private readonly url: string;
	private readonly authHeader: string;

	constructor(config: {apiKey: string; domain: string; region: string}) {
		this.apiKey = config.apiKey;
		this.domain = config.domain;
		this.region = config.region;

		this.url = this.createUrl();
		this.authHeader = this.createAuthHeader();
	}

	async sendEmail(data: SendMailParameters) {
		const formData = new URLSearchParams();
		formData.append('from', data.from);
		formData.append('to', data.to);
		formData.append('subject', data.subject);
		formData.append('text', data.text);

		const response = await fetch(this.url, {
			method: 'POST',
			body: formData,
			headers: {
				// eslint-disable-next-line @typescript-eslint/naming-convention
				Authorization: this.authHeader,
			},
		});

		if (!response.ok) {
			const details = {
				name: 'Mailgun',
				url: this.url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Mailgun: request failed with none ok status',
				details,
			);
		}
	}

	private createUrl() {
		if (this.region === 'eu') {
			return `https://api.eu.mailgun.net/v3/${this.domain}/messages`;
		}

		return `https://api.mailgun.net/v3/${this.domain}/messages`;
	}

	private createAuthHeader() {
		return `Basic ${Buffer.from(`api:${this.apiKey}`).toString('base64')}`;
	}
}
