import {MailgunEmailSender} from './mailgun.js';
import {MailgunConfig} from './mailgun-config.js';

export class MailgunFactory {
	create(environment: Record<string, unknown>) {
		const mailgunConfig = new MailgunConfig(environment);

		return new MailgunEmailSender(mailgunConfig);
	}
}
