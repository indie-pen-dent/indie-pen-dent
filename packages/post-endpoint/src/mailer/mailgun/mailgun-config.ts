type MailgunEnvironment = {
	apiKey: string;
	domain: string;
	region: string;
};

export class MailgunConfig {
	readonly apiKey: string;
	readonly domain: string;
	readonly region: string;

	constructor(environment: Record<string, unknown>) {
		const mailgunEnvironment = this.#validate(environment);

		this.apiKey = mailgunEnvironment.apiKey;

		this.domain = mailgunEnvironment.domain;

		this.region = mailgunEnvironment.region;
	}

	get type() {
		return 'mailgun';
	}

	#validate(environment: Record<string, unknown>): MailgunEnvironment {
		const result: Partial<MailgunEnvironment> = {};

		if (!this.#validateApiKey(environment.MAILGUN_API_KEY)) {
			throw new Error('Invalid MAILGUN_API_KEY');
		}

		result.apiKey = environment.MAILGUN_API_KEY;

		if (!this.#validateDomain(environment.MAILGUN_DOMAIN)) {
			throw new Error('Invalid MAILGUN_DOMAIN');
		}

		result.domain = environment.domain = environment.MAILGUN_DOMAIN;

		if (!this.#validateRegion(environment.MAILGUN_REGION)) {
			throw new Error('Invalid MAILGUN_REGION');
		}

		result.region = environment.MAILGUN_REGION;

		return result as MailgunEnvironment;
	}

	#validateApiKey(apiKey: unknown): apiKey is string {
		return typeof apiKey === 'string';
	}

	#validateDomain(domain: unknown): domain is string {
		if (typeof domain !== 'string') {
			return false;
		}

		try {
			new URL(`http://${domain}`);

			return true;
		} catch (error) {
			console.log(error);
			return false;
		}
	}

	#validateRegion(region: unknown): region is 'eu' | string {
		if (region === 'eu' || typeof region === 'string') {
			return true;
		}

		return false;
	}
}
