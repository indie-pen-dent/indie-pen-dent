export type SendMailParameters = {
	to: string;
	from: string;
	subject: string;
	text: string;
};

export type Mailer = {
	sendEmail: (data: SendMailParameters) => Promise<void>;
};

export type MailerFactoryFunction = () => Mailer;
