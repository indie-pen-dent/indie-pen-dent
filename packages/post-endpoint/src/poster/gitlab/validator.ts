import {isObject} from '../../utils/object.js';
import {ValidationError} from '../../errors/validation.js';

export type GitLabSettings = {
	branchName: string;
	apiKey: string;
	repositoryId: number;
	pipelineToken: string;
};

export class GitLabValidator {
	validate(data: unknown): data is GitLabSettings {
		const errors: string[] = [];

		if (!isObject(data)) {
			errors.push('Missing or incorrect GitLab data.');
			throw new ValidationError('Invalid Request', errors);
		}

		const branchNameErrors = this.#validateBranchName(data.branchName);

		errors.push(...branchNameErrors);

		const apiKeyErrors = this.#validateApiKey(data.apiKey);

		errors.push(...apiKeyErrors);

		const repositoryIdErrors = this.#validateRepositoryId(
			data.repositoryId,
		);

		errors.push(...repositoryIdErrors);

		const pipelineTokenErrors = this.#validatePipelineToken(
			data.pipelineToken,
		);

		errors.push(...pipelineTokenErrors);

		if (errors.length > 0) {
			throw new ValidationError('Invalid Request', errors);
		}

		return true;
	}

	#validateBranchName(branchName: unknown): string[] {
		if (typeof branchName !== 'string') {
			return ['Branch name must be a string.'];
		}

		return [];
	}

	#validateApiKey(apiKey: unknown): string[] {
		if (typeof apiKey !== 'string') {
			return ['API Key must be a string.'];
		}

		return [];
	}

	#validateRepositoryId(repositoryId: unknown): string[] {
		if (typeof repositoryId !== 'number') {
			return ['Repository ID must be a number.'];
		}

		return [];
	}

	#validatePipelineToken(pipelineToken: unknown): string[] {
		if (typeof pipelineToken !== 'string') {
			return ['Pipeline token must be a string.'];
		}

		return [];
	}
}

export const gitlabValidator = new GitLabValidator();
