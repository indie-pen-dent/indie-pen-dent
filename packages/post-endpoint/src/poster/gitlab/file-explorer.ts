import fetch from 'node-fetch';
import {type InFileExplorer} from '../types.js';
import {ThirdPartyError} from '../../errors/third-party.js';
import {type GitLabSettings} from './validator.js';

export class GitlabFileExplorer implements InFileExplorer {
	#settings: GitLabSettings;
	#apiUrl: string;

	constructor(settings: GitLabSettings) {
		this.#settings = settings;
		this.#apiUrl = `https://gitlab.com/api/v4/projects/${settings.repositoryId}`;
	}
	
	async getDirectory(filePath: string) {
		const headers = this.#getHeaders();
		const url = `${this.#apiUrl}/repository/files/${filePath}`;
		
		const response = await fetch(url, {
			method: 'GET',
			headers,
		});
		
		if (!response.ok) {
			const details = {
				name: 'gitlab',
				url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Gitlab: error getting directory.',
				details,
			);
		}

		return response.json();
	}

	async getFile(filePath: string) {
		return this.getDirectory(filePath);
	}

	async createFile(filePath: string, fileContent: string, commitMessage: string) {
		const headers = this.#getHeaders();
		const url = `${this.#apiUrl}/repository/files/${filePath}`;

		const response = await fetch(url, {
			method: 'POST',
			headers,
			body: JSON.stringify({
				branch: this.#settings.branchName,
				// eslint-disable-next-line @typescript-eslint/naming-convention
				commit_message: commitMessage,
				content: fileContent,
				// eslint-disable-next-line @typescript-eslint/naming-convention
				file_path: filePath,
				id: this.#settings.repositoryId,
			}),
		});

		if (!response.ok) {
			const details = {
				name: 'gitlab',
				url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Gitlab: error creating file.',
				details,
			);
		}

		return response.json();
	}
	
	async replaceFile(filePath: string, fileContent: string, commitMessage: string) {
		const headers = this.#getHeaders();
		const url = `${this.#apiUrl}/repository/files/${filePath}`;

		const response = await fetch(url, {
			method: 'PUT',
			headers,
			body: JSON.stringify({
				branch: this.#settings.branchName,
				// eslint-disable-next-line @typescript-eslint/naming-convention
				commit_message: commitMessage,
				content: fileContent,
				// eslint-disable-next-line @typescript-eslint/naming-convention
				file_path: filePath,
				id: this.#settings.repositoryId,
			}),
		});

		if (!response.ok) {
			const details = {
				name: 'gitlab',
				url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Gitlab: error updating file.',
				details,
			);
		}

		return response.json();
	}
	
	async deleteFile(filePath: string) {
		const headers = this.#getHeaders();
		const url = `${this.#apiUrl}/repository/files/${filePath}`;

		const response = await fetch(url, {
			method: 'DELETE',
			headers,
			body: JSON.stringify({
				branch: this.#settings.branchName,
				// eslint-disable-next-line @typescript-eslint/naming-convention
				commit_message: `DELETING: ${filePath}`,
				// eslint-disable-next-line @typescript-eslint/naming-convention
				file_path: filePath,
				id: this.#settings.repositoryId,
			}),
		});

		if (!response.ok) {
			const details = {
				name: 'gitlab',
				url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Gitlab: error deleting file.',
				details,
			);
		}

		return response.json();
		
	}

	#getHeaders() {
		return {
			'Content-Type': 'application/json',
			'PRIVATE-TOKEN': this.#settings.apiKey,
		};
	}
}
