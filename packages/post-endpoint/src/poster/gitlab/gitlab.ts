import {readFile} from 'node:fs/promises';
import {toBase64} from '../../common/string.js';
import {type GitlabFileExplorer} from './file-explorer.js';

export class GitLabFileUploader {
	#fileExplorer: GitlabFileExplorer;

	constructor(fileExplorer: GitlabFileExplorer) {
		this.#fileExplorer = fileExplorer;
	}

	async uploadImage(fileName: string, contentPath: string): Promise<boolean> {
		const fileContent = await this.#readFile(contentPath);
		const encodedContent = toBase64(fileContent);

		const commitName = `New Image: ${fileName}`;

		const filePath = `static/images/${fileName}`;

		await this.#fileExplorer.createFile(encodedContent, commitName, filePath);

		return true;
	}

	async uploadPost(postName: string, postContent: string): Promise<boolean> {
		const encodedContent = toBase64(postContent);

		const commitMessage = `New Post: ${postName}`;

		const filePath = `content/post/${postName}.md`;

		await this.#fileExplorer.createFile(encodedContent, commitMessage, filePath);

		return true;
	}

	async replacePost(fileName: string, postName: string, postContent: string) {
		const encodedContent = toBase64(postContent);

		const commitMessage = `Updating Post: ${postName}`;

		const filePath = `content/post/${fileName}`;

		await this.#fileExplorer.replaceFile(filePath, encodedContent, commitMessage);

		return true;
	}

	async #readFile(filePath: string): Promise<Buffer> {
		return readFile(filePath);
	}
}
