export type GitLabSettings = {
	branchName: string;
	apiKey: string;
	repositoryId: number;
	pipelineToken: string;
};

type EncryptionHelper = {
	encrypt: (data: string) => string;
	decrypt: (data: string) => string;
};

export class Settings {
	#branchName: string | undefined;
	#apiKey: string | undefined;
	#repositoryId: number | undefined;
	#pipelineToken: string | undefined;

	#encryptionHelper: EncryptionHelper;

	constructor(encryptionHelper: EncryptionHelper) {
		this.#encryptionHelper = encryptionHelper;
	}

	get branchName(): string {
		if (!this.#branchName) {
			throw new Error('No branch name.');
		}

		return this.#branchName;
	}

	get apiKey(): string {
		if (!this.#apiKey) {
			throw new Error('No API Key.');
		}

		return this.#apiKey;
	}

	get repositoryId(): number {
		if (this.#repositoryId === undefined) {
			throw new Error('No Repository ID.');
		}

		return this.#repositoryId;
	}

	get pipelineToken(): string {
		if (!this.#pipelineToken) {
			throw new Error('No Pipeline Token.');
		}

		return this.#pipelineToken;
	}

	fromStore(settings: GitLabSettings): this {
		this.#branchName = settings.branchName;
		this.#repositoryId = settings.repositoryId;
		this.#apiKey = this.#encryptionHelper.decrypt(settings.apiKey);
		this.#pipelineToken = this.#encryptionHelper.decrypt(
			settings.pipelineToken,
		);

		return this;
	}

	fromClient(settings: GitLabSettings): this {
		this.#branchName = settings.branchName;
		this.#repositoryId = settings.repositoryId;
		this.#apiKey = settings.apiKey;
		this.#pipelineToken = settings.pipelineToken;

		return this;
	}

	toStore(): GitLabSettings {
		return {
			branchName: this.branchName,
			repositoryId: this.repositoryId,
			apiKey: this.#encryptionHelper.encrypt(this.apiKey),
			pipelineToken: this.#encryptionHelper.encrypt(this.pipelineToken),
		};
	}

	toClient(): Omit<GitLabSettings, 'apiKey' | 'pipelineToken'> {
		return {
			branchName: this.branchName,
			repositoryId: this.repositoryId,
		};
	}
}
