import {encryptionHelper} from '../../encrypter/encrypter.js';
import {type Host} from '../types.js';
import {type GitLabSettings} from './validator.js';
import {GitlabFileExplorer} from './file-explorer.js';
import {gitlabValidator} from './validator.js';
import {GitLabFileUploader} from './gitlab.js';
import {Settings} from './settings.js';

export class GitLab implements Host<GitLabSettings> {
	settings: Settings;
	#fileExplorer: GitlabFileExplorer | undefined;
	#poster: GitLabFileUploader | undefined;

	constructor() {
		this.settings = new Settings(encryptionHelper);
	}

	addSettingsFromStore(settings: unknown): Readonly<GitLabSettings> {
		if (!gitlabValidator.validate(settings)) {
			throw new Error('Not possible.');
		}

		this.settings.fromStore(settings);
		this.#fileExplorer = new GitlabFileExplorer(this.settings);
		this.#poster = new GitLabFileUploader(this.#fileExplorer);

		return this.settings;
	}

	addSettingsFromClient(settings: unknown): Readonly<GitLabSettings> {
		if (!gitlabValidator.validate(settings)) {
			throw new Error('Not possible.');
		}

		this.settings.fromClient(settings);
		this.#fileExplorer = new GitlabFileExplorer(this.settings);
		this.#poster = new GitLabFileUploader(this.#fileExplorer);

		return this.settings;
	}

	getSettingsForClient(): Record<string, unknown> {
		return this.settings.toClient();
	}

	getSettingsForStore(): Record<string, unknown> {
		return this.settings.toStore() as unknown as Record<string, unknown>;
	}

	async uploadPost(postName: string, postContent: string): Promise<boolean> {
		if (!this.#poster) {
			throw new Error('uploadPost called before adding settings.');
		}

		return this.#poster.uploadPost(postName, postContent);
	}

	async uploadImage(fileName: string, contentPath: string): Promise<boolean> {
		if (!this.#poster) {
			throw new Error('uploadImage called before adding settings.');
		}

		return this.#poster.uploadImage(fileName, contentPath);
	}
	
	async getPosts() {
		if (!this.#fileExplorer) {
			throw new Error('getPosts called before adding settings.');
		}
		
		return this.#fileExplorer.getDirectory('content/post/');
	}
	
	async getImages() {
		if (!this.#fileExplorer) {
			throw new Error('getImages called before adding settings.');
		}
		
		return this.#fileExplorer.getDirectory('static/images/');
	}

	async getPost(filePath: string) {
		if (!this.#fileExplorer) {
			throw new Error('getPost called before adding settings.');
		}
		
		return this.#fileExplorer.getFile(`content/post/${filePath}`);
	}

	async getImage(filePath: string) {
		if (!this.#fileExplorer) {
			throw new Error('getImage called before adding settings.');
		}

		return this.#fileExplorer.getFile(`static/images/${filePath}`);
	}
	
	async deletePost(filePath: string) {
		if (!this.#fileExplorer) {
			throw new Error('deletePost called before adding settings.');
		}
		
		return this.#fileExplorer.deleteFile(`content/post/${filePath}`);
	}

	async deleteImage(filePath: string) {
		if (!this.#fileExplorer) {
			throw new Error('deleteImage called before adding settings.');
		}

		return this.#fileExplorer.deleteFile(`static/images/${filePath}`);
	}

	async replacePost(filePath: string, postName: string, postContent: string): Promise<boolean> {
		if (!this.#fileExplorer || !this.#poster) {
			throw new Error('replacePost called before adding settings.');
		}

		return this.#poster.replacePost(filePath, postName, postContent);
	}
} 
