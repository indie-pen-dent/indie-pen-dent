export type Host<S> = {
	uploadPost: (postName: string, postContent: string) => Promise<boolean>;

	uploadImage: (fileName: string, contentPath: string) => Promise<boolean>;

	getPosts: () => Promise<unknown>;
	
	getImages: () => Promise<unkonwn>;

	getPost: (filePath: string) => Promise<unknown>;

	getImage: (filePath: string) => Promise<unknown>;

	deletePost: (filePath: string) => Promise<unknown>;

	deleteImage: (filePath: string) => Promise<unknown>;

	replacePost: (filePath: string, postName: string, postContent: string) => Promise<unknown>;

	addSettingsFromStore: (settings: unknown) => Readonly<S>;

	addSettingsFromClient: (settings: unknown) => Readonly<S>;

	getSettingsForStore: () => Record<string, unknown>;

	getSettingsForClient: () => Record<string, unknown>;
};

export type InFileExplorer = {
	getDirectory: (filePath: string) => Promise<unknown>;
	getFile: (filePath: string) => Promise<unknown>;
	createFile: (filePath: string, fileContent: string, commitMessage: string) => Promise<unkonwn>;
	replaceFile: (filePath: string, fileContent: string, commitMessage: string) => Promise<unknown>;
	deleteFile: (filePath: string) => Promise<unknown>;
};

export type HeadData = {
	date: string;
	author: string;
	title?: string;
};

export type PostDownload = {
	name: string;
	path: string;
	sha: string;
	type: 'file';
	content: {
		headData: HeadData;
		content: string;
	};
};
