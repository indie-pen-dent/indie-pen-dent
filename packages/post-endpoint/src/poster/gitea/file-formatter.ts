import {isValidDate} from '../../common/date.js';
import {parseBase64} from '../../common/string.js';
import type {HeadData, PostDownload} from '../types.js';
import type {Directory} from './schemas/directory.js';
import type {File} from './schemas/file.js';

export class FileFormatter {
	formatFile(fileData: File): PostDownload {
		const content = this.#formatContent(fileData.content);
		const headData = content.headData;

		if (!this.#isValidateHeadData(headData)) {
			throw new Error('Unable to process post head data.');
		}

		return {
			name: fileData.name,
			path: fileData.path,
			sha: fileData.sha,
			type: fileData.type,
			content: {
				headData,
				content: content.content,
			},
		};
	}

	formatDirectory(directoryData: Directory): Directory {
	  return directoryData.map((dir) => ({
	    name: dir.name,
	    path: dir.path,
	    sha: dir.sha,
	    type: dir.type,
	  }));
	}

	#formatContent(content: string): {headData: unknown; content: string} {
		const rawContent = parseBase64(content);

		const [headData, ...contentArray] = rawContent.split('}\n');

		const reAssembledContent = contentArray.join('}\n');

		const parsedHeadData = JSON.parse(headData + '}') as unknown;

		return {
			headData: parsedHeadData,
			content: reAssembledContent,
		};
	}

	#isValidateHeadData(headData: unknown): headData is HeadData {
		if (typeof headData !== 'object' || Array.isArray(headData) || headData === null) {
			return false;
		}

		if (!('date' in headData) || !isValidDate(headData.date)) {
			return false;
		}

		if (!('author' in headData) || typeof headData.author !== 'string') {
			return false;
		}
		
		if ('title' in headData && typeof headData.author !== 'string') {
			return false;
		}

		return true;
	}
}
