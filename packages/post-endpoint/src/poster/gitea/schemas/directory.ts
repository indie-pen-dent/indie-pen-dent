export type Directory = Array<{
	name: string;
	path: string;
	sha: string;
	type: 'dir' | 'file';
}>;

export const directorySchema = {
	type: 'array',
	items: {
		type: 'object',
		properties: {
			name: {
				type: 'string',
			},
			path: {
				type: 'string',
			},
			sha: {
				type: 'string',
			},
			type: {
				type: 'string',
				enum: ['file', 'dir'],
			},
		},
		required: ['name', 'path', 'sha', 'type'],
	},
};
