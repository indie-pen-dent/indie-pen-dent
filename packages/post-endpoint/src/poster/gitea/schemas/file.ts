export type File = {
	name: string;
	path: string;
	sha: string;
	type: 'file';
	content: string;
};

export const fileSchema = {
	type: 'object',
	properties: {
		name: {
			type: 'string',
		},
		path: {
			type: 'string',
		},
		sha: {
			type: 'string',
		},
		type: {
			type: 'string',
			const: 'file',
		},
		content: {
			type: 'string',
		},
	},
	required: ['name', 'path', 'sha', 'type', 'content'],
};
