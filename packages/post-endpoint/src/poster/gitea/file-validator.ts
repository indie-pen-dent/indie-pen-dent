import type {IntSchemaValidatorFactory, IntSchemaValidator} from '../../schema-validator/types.js';

export class FileValidator<File, Directory> {
	#fileValidator: IntSchemaValidator<File>;
	#directoryValidator: IntSchemaValidator<Directory>;

	constructor(
		fileSchema: Record<string, unknown>,
		directorySchema: Record<string, unknown>,
		validatorFactory: IntSchemaValidatorFactory,
	) {
		this.#fileValidator = validatorFactory.build(fileSchema);
		this.#directoryValidator = validatorFactory.build(directorySchema);
	}

	validateFile(file: unknown): file is File {
	  return this.#fileValidator.validate(file);
	}

	validateDirectory(directory: unknown): directory is Directory {
	  return this.#directoryValidator.validate(directory);
	}
}
