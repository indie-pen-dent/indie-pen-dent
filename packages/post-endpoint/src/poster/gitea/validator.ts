import {isObject} from '../../utils/object.js';
import {ValidationError} from '../../errors/validation.js';

export type GiteaSettings = {
	apiUrl: string;
	apiKey: string;
	owner: string;
	repositoryId: string;
	branchName: string;
};

export class GiteaValidator {
	validate(data: unknown): data is GiteaSettings {
		const errors: string[] = [];

		if (!isObject(data)) {
			errors.push('Missing or incorrect Gitea data.');
			throw new ValidationError('Invalid Request', errors);
		}

		const branchNameErrors = this.#validateBranchName(data.branchName);

		errors.push(...branchNameErrors);

		const apiKeyErrors = this.#validateApiKey(data.apiKey);

		errors.push(...apiKeyErrors);

		const repositoryIdErrors = this.#validateRepositoryId(
			data.repositoryId,
		);

		errors.push(...repositoryIdErrors);

		const apiUrlErrors = this.#validateApiUrl(data.apiUrl);

		errors.push(...apiUrlErrors);

		const ownerErrors = this.#validateOwner(data.owner);

		errors.push(...ownerErrors);

		if (errors.length > 0) {
			throw new ValidationError('Invalid Request', errors);
		}

		return true;
	}

	#validateBranchName(branchName: unknown): string[] {
		if (typeof branchName !== 'string') {
			return ['Branch name must be a string.'];
		}

		return [];
	}

	#validateApiKey(apiKey: unknown): string[] {
		if (typeof apiKey !== 'string') {
			return ['API Key must be a string.'];
		}

		return [];
	}

	#validateRepositoryId(repositoryId: unknown): string[] {
		if (typeof repositoryId !== 'string') {
			return ['Repository ID must be a string.'];
		}

		return [];
	}

	#validateApiUrl(apiUrl: unknown): string[] {
		if (typeof apiUrl !== 'string') {
			return ['Api Url must be a string.'];
		}

		return [];
	}

	#validateOwner(owner: unknown): string[] {
		if (typeof owner !== 'string') {
			return ['Owner must be a string.'];
		}

		return [];
	}
}

export const giteaValidator = new GiteaValidator();
