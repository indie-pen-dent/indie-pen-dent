import fetch, {Headers} from 'node-fetch';
import {type InFileExplorer} from '../types.js';
import {ThirdPartyError} from '../../errors/third-party.js';
import {type GiteaSettings} from './validator.js';

export class GiteaFileExplorer implements InFileExplorer {
	#apiUrl: string;
	#apiToken: string;
	#owner: string;
	#repo: string;
	#branch: string;

	constructor(settings: GiteaSettings) {
		this.#apiUrl = settings.apiUrl + '/api/v1';
		this.#apiToken = settings.apiKey;
		this.#owner = settings.owner;
		this.#repo = settings.repositoryId;
		this.#branch = settings.branchName;
	}

	async getDirectory(filePath: string): Promise<unknown> {
		const url = this.#createUrl(filePath);
		const headers = this.#getHeaders();

		const response = await fetch(url, {
		  method: 'GET',
		  headers,
		});

		if (!response.ok) {
		  const details = {
		    name: 'gitea',
		    url,
		    status: response.status,
		    responseBody: await response.text(),
		  };

		  throw new ThirdPartyError(
		    'Gitea: error getting directory.',
		    details,
		  );
		}

		const responseJson = await response.json();

		return responseJson;
	}

	async getFile(filePath: string): Promise<unknown> {
		return this.getDirectory(filePath);
	}

	async createFile(filePath: string, fileContent: string, commitMessage: string): Promise<unknown> {
		const url = this.#createUrl(filePath);
		const headers = this.#getHeaders();
		const data = {
			message: commitMessage,
			content: fileContent,
			branch: this.#branch,
		};

		const response = await fetch(url, {
			method: 'POST',
			headers,
			body: JSON.stringify(data),
		});

		if (!response.ok) {
			const details = {
				name: 'gitea',
				url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Gitea: error creating file',
				details,
			);
		}

		return response.json();
	}

	async replaceFile(filePath: string, fileContent: string, commitMessage: string): Promise<unknown> {
		const url = this.#createUrl(filePath);
		const headers = this.#getHeaders();
		const data = {
			message: commitMessage,
			content: fileContent,
			branch: this.#branch,
		};

		const response = await fetch(url, {
			method: 'PUT',
			headers,
			body: JSON.stringify(data),
		});

		if (!response.ok) {
			const details = {
				name: 'gitea',
				url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Gitea: error replacing file.',
				details,
			);
		}

		return response.json();
	}

	async deleteFile(filePath: string): Promise<void> {
		const url = this.#createUrl(filePath);
		const headers = this.#getHeaders();
		const data = {
			branch: this.#branch,
			message: `DELETING: ${filePath}`,
		};

		const response = await fetch(url, {
			method: 'Delete',
			headers,
			body: JSON.stringify(data),
		});

		if (!response.ok) {
			const details = {
				name: 'gitea',
				url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Gitea: error deleting file.',
				details,
			);
		}
	}
	
	#getHeaders(): Headers {
		const headers = new Headers();
		headers.set('Authorization', `token ${this.#apiToken}`);
		return headers;
	}

	#createUrl(filePath: string) {
		const encodedFilePath = encodeURIComponent(filePath);
		return `${this.#apiUrl}/repos/${this.#owner}/${this.#repo}/contents/${encodedFilePath}`;
	}
}
