export type GiteaSettings = {
	apiUrl: string;
	apiKey: string;
	owner: string;
	repositoryId: string;
	branchName: string;
};

type EncryptionHelper = {
	encrypt: (data: string) => string;
	decrypt: (data: string) => string;
};

export class Settings {
	#apiKey: string | undefined;
	#repositoryId: string | undefined;
	#branchName: string | undefined;
	#apiUrl: string | undefined;
	#owner: string | undefined;

	#encryptionHelper: EncryptionHelper;

	constructor(encryptionHelper: EncryptionHelper) {
		this.#encryptionHelper = encryptionHelper;
	}

	get branchName(): string {
		if (!this.#branchName) {
			throw new Error('No branch name.');
		}

		return this.#branchName;
	}

	get apiKey(): string {
		if (!this.#apiKey) {
			throw new Error('No API Key.');
		}

		return this.#apiKey;
	}

	get repositoryId(): string {
		if (this.#repositoryId === undefined) {
			throw new Error('No Repository ID.');
		}

		return this.#repositoryId;
	}

	get apiUrl(): string {
		if (!this.#apiUrl) {
			throw new Error('No API URL.');
		}

		return this.#apiUrl;
	}

	get owner(): string {
		if (!this.#owner) {
			throw new Error('No Owner.');
		}

		return this.#owner;
	}

	fromStore(settings: GiteaSettings): this {
		this.#branchName = settings.branchName;
		this.#repositoryId = settings.repositoryId;
		this.#apiKey = this.#encryptionHelper.decrypt(settings.apiKey);
		this.#apiUrl = settings.apiUrl;
		this.#owner = settings.owner;

		return this;
	}

	fromClient(settings: GiteaSettings): this {
		this.#branchName = settings.branchName;
		this.#repositoryId = settings.repositoryId;
		this.#apiKey = settings.apiKey;
		this.#apiUrl = settings.apiUrl;
		this.#owner = settings.owner;

		return this;
	}

	toStore(): GiteaSettings {
		return {
			branchName: this.branchName,
			repositoryId: this.repositoryId,
			apiKey: this.#encryptionHelper.encrypt(this.apiKey),
			apiUrl: this.apiUrl,
			owner: this.owner,
		};
	}

	toClient(): Omit<GiteaSettings, 'apiKey'> {
		return {
			branchName: this.branchName,
			repositoryId: this.repositoryId,
			apiUrl: this.apiUrl,
			owner: this.owner,
		};
	}
}
