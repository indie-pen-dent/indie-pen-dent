import {encryptionHelper} from '../../encrypter/encrypter.js';
import {SchemaValidatorFactory} from '../../schema-validator/schema-validator.js';
import {ThirdPartyError} from '../../errors/third-party.js';
import {type Host} from '../types.js';
import {GiteaFileUploader} from './gitea.js';
import {type GiteaSettings, giteaValidator} from './validator.js';
import {Settings} from './settings.js';
import {GiteaFileExplorer} from './file-explorer.js';
import {FileValidator} from './file-validator.js';
import {FileFormatter} from './file-formatter.js';
import {directorySchema, type Directory} from './schemas/directory.js';
import {fileSchema, type File} from './schemas/file.js';

export class Gitea implements Host<GiteaSettings> {
	settings: Settings;
	#poster: GiteaFileUploader | undefined;
	#fileExplorer: GiteaFileExplorer | undefined;
	#fileValidator: FileValidator<File, Directory> | undefined;

	constructor() {
		this.settings = new Settings(encryptionHelper);
	}

	addSettingsFromStore(settings: unknown): Readonly<GiteaSettings> {
		if (!giteaValidator.validate(settings)) {
			throw new Error('Not possible.');
		}

		this.settings.fromStore(settings);
		this.#fileExplorer = new GiteaFileExplorer(this.settings);
		this.#poster = new GiteaFileUploader(this.#fileExplorer);

		return this.settings;
	}

	addSettingsFromClient(settings: unknown): Readonly<GiteaSettings> {
		if (!giteaValidator.validate(settings)) {
			throw new Error('Not possible.');
		}

		this.settings.fromClient(settings);
		this.#fileExplorer = new GiteaFileExplorer(this.settings);
		this.#poster = new GiteaFileUploader(this.#fileExplorer);

		return this.settings;
	}

	getSettingsForClient(): Record<string, unknown> {
		return this.settings.toClient();
	}

	getSettingsForStore(): Record<string, unknown> {
		return this.settings.toStore() as unknown as Record<string, unknown>;
	}

	async uploadPost(postName: string, postContent: string): Promise<boolean> {
		if (!this.#poster) {
			throw new Error('uploadPost called before adding settings.');
		}

		return this.#poster.uploadPost(postName, postContent);
	}

	async uploadImage(fileName: string, contentPath: string): Promise<boolean> {
		if (!this.#poster) {
			throw new Error('uploadImage called before adding settings.');
		}

		return this.#poster.uploadImage(fileName, contentPath);
	}

	async getPosts() {
		if (!this.#fileExplorer) {
			throw new Error('getPosts called before adding settings.');
		}
		
		const directory = await this.#fileExplorer.getDirectory('content/post/');

		const validator = this.#getValidator();

		if (!validator.validateDirectory(directory)) {
			throw new ThirdPartyError('Invalid Directory Response.', {
				name: 'Gitea',
				url: 'content/post/',
				status: 200,
				responseBody: JSON.stringify(directory),
			});
		}

		const formatter = this.#getFileFormatter();

		return formatter.formatDirectory(directory);
	}
	
	async getImages() {
		if (!this.#fileExplorer) {
			throw new Error('getImages called before adding settings.');
		}
		
		const directory = await this.#fileExplorer.getDirectory('static/images/');

		const validator = this.#getValidator();

		if (!validator.validateDirectory(directory)) {
			throw new ThirdPartyError('Invalid Directory Response.', {
				name: 'Gitea',
				url: 'static/images/',
				status: 200,
				responseBody: JSON.stringify(directory),
			});
		}

		const formatter = this.#getFileFormatter();

		return formatter.formatDirectory(directory);
	}

	async getPost(filePath: string) {
		if (!this.#fileExplorer) {
			throw new Error('getPost called before adding settings.');
		}

		const url = `content/post/${filePath}`;
		
		const post = await this.#fileExplorer.getFile(url);

		const validator = this.#getValidator();

		if (!validator.validateFile(post)) {
			throw new ThirdPartyError('Invalid File Response.', {
				name: 'Gitea',
				url,
				status: 200,
				responseBody: JSON.stringify(post),
			});
		}

		const formatter = this.#getFileFormatter();

		return formatter.formatFile(post);
	}

	async getImage(filePath: string) {
		if (!this.#fileExplorer) {
			throw new Error('getImage called before adding settings.');
		}

		const url = `static/images/${filePath}`;

		const image = await this.#fileExplorer.getFile(url);

		const validator = this.#getValidator();

		if (!validator.validateFile(image)) {
			throw new ThirdPartyError('Invalid File Response.', {
				name: 'Gitea',
				url,
				status: 200,
				responseBody: JSON.stringify(image),
			});
		}

		const formatter = this.#getFileFormatter();

		return formatter.formatFile(image);
	}
	
	async deletePost(filePath: string) {
		if (!this.#fileExplorer) {
			throw new Error('deletePost called before adding settings.');
		}
		
		return this.#fileExplorer.deleteFile(`content/post/${filePath}`);
	}

	async deleteImage(filePath: string) {
		if (!this.#fileExplorer) {
			throw new Error('deleteImage called before adding settings.');
		}

		return this.#fileExplorer.deleteFile(`static/images/${filePath}`);
	}

	async replacePost(filePath: string, postName: string, postContent: string): Promise<boolean> {
		if (!this.#fileExplorer || !this.#poster) {
			throw new Error('replacePost called before adding settings.');
		}

		return this.#poster.replacePost(filePath, postName, postContent);
	}

	#getValidator() {
		if (this.#fileValidator === undefined) {
			const schemaValidatorFactory = this.#getSchemaValidatorFactory();
			this.#fileValidator = new FileValidator<File, Directory>(fileSchema, directorySchema, schemaValidatorFactory);
		}

		return this.#fileValidator;
	}

	#getSchemaValidatorFactory() {
		return new SchemaValidatorFactory();
	}

	#getFileFormatter() {
		return new FileFormatter();
	}
} 
