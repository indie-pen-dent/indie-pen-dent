import {readFile} from 'node:fs/promises';
import {type InFileExplorer} from '../types.js';

export class GiteaFileUploader {
	#fileExplorer: InFileExplorer;

	constructor(fileExplorer: InFileExplorer) {
		this.#fileExplorer = fileExplorer;
	}

	async uploadPost(postName: string, postContent: string): Promise<boolean> {
		const encodedContent = Buffer.from(postContent).toString('base64');

		const commitMessage = `New Post: ${postName}`;

		const filePath = `content/post/${postName}.md`;

		await this.#fileExplorer.createFile(
			filePath,
			encodedContent,
			commitMessage,
		);

		return true;
	}

	async uploadImage(fileName: string, contentPath: string): Promise<boolean> {
		const fileContent = await this.#readFile(contentPath);
		const encodedContent = Buffer.from(fileContent).toString('base64');

		const commitName = `[skip ci] New Image: ${fileName}`;

		const filePath = `static/images/${fileName}`;

		await this.#fileExplorer.createFile(filePath, encodedContent, commitName);

		return true;
	}

	async replacePost(fileName: string, postName: string, postContent: string): Promise<boolean> {
		const encodedContent = Buffer.from(postContent).toString('base64');

		const commitMessage = `Updating Post: ${postName}`;

		const filePath = `content/post/${fileName}`;

		await this.#fileExplorer.replaceFile(filePath, encodedContent, commitMessage);
		
		return true;
	}

	async #readFile(filePath: string): Promise<Buffer> {
		return readFile(filePath);
	}
}
