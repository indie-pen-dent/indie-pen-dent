import type {Host} from './types.js';

export class HostFactory {
	#hosts: Record<string, () => Host<unknown>>;

	constructor(hosts: Record<string, () => Host<unknown>>) {
		this.#hosts = hosts;
	}

	get(hostType: string): Host<unknown> {
		const hostBuilder = this.#getHostBuilder(hostType);

		return hostBuilder();
	}

	#getHostBuilder(hostType: string): () => Host<unknown> {
		const hostBuilder = this.#hosts[hostType];

		if (!hostBuilder) {
			throw new Error(
				`Unknown host service: ${JSON.stringify(hostType)}.`,
			);
		}

		return hostBuilder;
	}
}
