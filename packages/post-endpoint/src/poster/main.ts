import {hostHandlers} from './hosts.js';
import {HostFactory} from './host-factory.js';

export const hostFactory = new HostFactory(hostHandlers);
