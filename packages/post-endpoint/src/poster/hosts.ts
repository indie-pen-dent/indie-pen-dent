import {GitLab} from './gitlab/main.js';
import {Gitea} from './gitea/main.js';
import type {Host} from './types.js';

export const hostHandlers: Record<string, () => Host<unknown>> = {
	// eslint-disable-next-line @typescript-eslint/naming-convention
	GitLab: () => new GitLab(),
	// eslint-disable-next-line @typescript-eslint/naming-convention
	Gitea: () => new Gitea(),
	// eslint-disable-next-line @typescript-eslint/naming-convention
	Skribilo: () => new Gitea(),
};
