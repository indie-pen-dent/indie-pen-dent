import crypto from 'node:crypto';
import {jwtSecret} from '../config.js';

class EncryptionHelper {
	private readonly algorithm: string;
	private readonly key: string;

	constructor(key: string) {
		this.algorithm = 'aes-256-cbc';
		this.key = key;
	}

	encrypt(text: string): string {
		const iv = this.#generateIv();

		const cipher = crypto.createCipheriv(this.algorithm, this.key, iv);

		let encrypted = cipher.update(text, 'utf8', 'hex');
		encrypted += cipher.final('hex');

		return iv.toString('hex') + ':' + encrypted;
	}

	decrypt(encryptedText: string): string {
		const [ivString, text] = encryptedText.split(':');
		const iv = Buffer.from(ivString, 'hex');

		const decipher = crypto.createDecipheriv(this.algorithm, this.key, iv);

		let decrypted = decipher.update(text, 'hex', 'utf8');
		decrypted += decipher.final('utf8');

		return decrypted;
	}

	#generateIv() {
		return crypto.randomBytes(16);
	}
}

export const encryptionHelper = new EncryptionHelper(jwtSecret.slice(0, 32));
