import {MongoClient} from 'mongodb';
import {databaseUrl} from '../config.js';
import {MongoDbRepository} from './mongodb-repository.js';
import {type User, type HostedBlog, type InEntity} from './types.js';

export class Store {
	users: MongoDbRepository<User>;
	hostedBlogs: MongoDbRepository<HostedBlog>;
	skribiloBlogs: MongoDbRepository<InEntity>;

	constructor(databaseUrl: string) {
		const client = this.createConnection(databaseUrl);

		this.users = new MongoDbRepository<User>(client, 'users');
		this.hostedBlogs = new MongoDbRepository<HostedBlog>(client, 'blogs');
		this.skribiloBlogs = new MongoDbRepository<InEntity>(
			client,
			'skribiloblogs',
		);
	}

	createConnection(databaseUrl: string) {
		return new MongoClient(databaseUrl);
	}
}

export const store = new Store(databaseUrl);
