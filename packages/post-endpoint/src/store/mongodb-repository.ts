import {
	type Collection,
	type WithId,
	type Filter,
	type InsertOneResult,
	type UpdateResult,
	type MongoClient,
	type OptionalUnlessRequiredId,
} from 'mongodb';
import {type InEntity} from './types.js';

export class MongoDbRepository<T extends InEntity> {
	private readonly collection: Collection<T>;

	constructor(private readonly client: MongoClient, private readonly collectionName: string) {
		const database = this.client.db();
		this.collection = database.collection<T>(this.collectionName);
	}

	async getAll(): Promise<Array<WithId<T>>> {
		const result = await this.collection.find({}).toArray();
		return result;
	}

	async getById(id: string): Promise<WithId<T> | undefined> {
		// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
		const query: Filter<T> = {id} as Filter<T>;
		const result = await this.collection.findOne(query);
		return result ?? undefined;
	}

	async create(entity: T): Promise<T> {
		const result: InsertOneResult<any> = await this.collection.insertOne(
			entity as OptionalUnlessRequiredId<T>,
		);

		entity._id = result.insertedId.toString();
		return entity;
	}

	async update(
		id: string,
		updatedEntity: Partial<T>,
	): Promise<T | undefined> {
		// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
		const query: Filter<T> = {id} as Filter<T>;

		const result: UpdateResult = await this.collection.updateOne(query, {
			$set: updatedEntity,
		});

		if (result.modifiedCount > 0) {
			return updatedEntity as T;
		}
	}

	async upsert(
		id: string,
		updatedEntity: Partial<T>,
	): Promise<T | undefined> {
		// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
		const query: Filter<T> = {id} as Filter<T>;

		const result: UpdateResult = await this.collection.updateOne(
			query,
			{
				$set: updatedEntity,
			},
			{upsert: true},
		);

		if (result.modifiedCount > 0) {
			return updatedEntity as T;
		}
	}

	async delete(id: string): Promise<boolean> {
		// eslint-disable-next-line @typescript-eslint/consistent-type-assertions
		const query: Filter<T> = {id} as Filter<T>;
		const result = await this.collection.deleteOne(query);
		return result.deletedCount > 0;
	}
}
