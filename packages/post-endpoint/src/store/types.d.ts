export type InEntity = {
	_id?: string;
	id: string;
};

export type User = {
	gitService: string;
	settings: Record<string, unknown>;
} & InEntity;

export type HostedBlog = {
	pinnedHash: string;
} & InEntity;

export type SkribiloBlog = Record<string, unknown> & InEntity;
