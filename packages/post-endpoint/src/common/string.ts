import {Buffer} from 'node:buffer';

export function toBase64(inputString: Buffer | string): string {
	return Buffer.from(inputString).toString('base64');
}

export function parseBase64(inputString: string): string {
	return Buffer.from(inputString, 'base64').toString();
}
