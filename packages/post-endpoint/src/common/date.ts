export function isValidDate(date: unknown): boolean {
	if (date instanceof Date && !Number.isNaN(date.valueOf())) {
		return true;
	}

	if (typeof date === 'string' && isValidDate(new Date(date))) {
		return true;
	}

	return false;
}
