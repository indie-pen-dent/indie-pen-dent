export class JunctionClass<T> {
	#name: string;
	#factories: Record<string, () => T>;

	constructor(name: string, factories: Record<string, () => T>) {
		this.#name = name;
		this.#factories = factories;
	}

	get(type: unknown) {
		if (typeof type !== 'string' || !this.#factories[type]) {
			throw new Error(`Invalid ${this.#name}: "${String(type)}".`);
		}

		return this.#factories[type]();
	}
}
