export function isRecord(t: unknown): t is Record<string, unknown> {
	const isObject = typeof t === 'object';

	const isArray = Array.isArray(t);

	return isObject && !isArray;
}
