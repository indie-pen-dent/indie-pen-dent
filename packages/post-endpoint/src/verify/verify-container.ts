import {Verifier} from './verifier.js';
import {CodeGenerator} from './code-generator.js';

export class VerifyContainer {
	#verifier?: Verifier;

	getVerifier() {
		if (this.#verifier) {
			return this.#verifier;
		}

		this.#verifier = new Verifier(this.getCodeGenerator());

		return this.#verifier;
	}

	getCodeGenerator() {
		return new CodeGenerator();
	}
}

export const verifyContainer = new VerifyContainer();
