export class CodeGenerator {
	generate(): string {
		const randomNumber = Math.floor(Math.random() * 1_000_000);

		const randomString = String(randomNumber);

		return randomString.padEnd(0);
	}
}
