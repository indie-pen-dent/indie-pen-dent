type Timer = ReturnType<typeof setTimeout>;

type Account = {
	code: string;
	timeout: Timer;
	blogName: string;
};

export class Verifier {
	#accounts = new Map<string, Account>();
	#codeGenerator: {generate: () => string};

	constructor(codeGenerator: {generate: () => string}) {
		this.#codeGenerator = codeGenerator;
	}

	startVerification(email: string, blogName: string): string {
		this.#clearExistingAccount(email);

		const code = this.#codeGenerator.generate();
		const timeout = setTimeout(() => this.#accounts.delete(email), 900_000);

		this.#accounts.set(email, {code, timeout, blogName});

		return code;
	}

	verifyCode(email: string, code: string): string | undefined {
		const account = this.#getAccount(email);

		if (!account) {
			return;
		}

		if (account.code !== code) {
			return;
		}

		return account.blogName;
	}

	#clearExistingAccount(email: string): void {
		const existingAccount = this.#getAccount(email);

		if (!existingAccount) {
			return;
		}

		clearTimeout(existingAccount.timeout);
	}

	#getAccount(email: string): Account | undefined {
		return this.#accounts.get(email);
	}
}
