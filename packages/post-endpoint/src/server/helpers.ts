import type {Response} from 'node-fetch';

export async function responseParser(response: Response) {
	const contentType = response.headers.get('Content-Type');

	if (!contentType) {
		throw new Error('No Content-Type header');
	}

	if (contentType.includes('application/json')) {
		return response.json();
	}

	if (contentType.includes('application/x-www-form-urlencoded')) {
		const contentFormData = await response.formData();

		const responseObject: Record<string, string> = {};
		for (const [key, value] of contentFormData.entries()) {
			if (typeof value !== 'string') {
				throw new TypeError('Invalid data in response form data.');
			}

			responseObject[key] = value;
		}

		return responseObject;
	}

	throw new Error(`Unhandled content type: ${contentType}.`);
}
