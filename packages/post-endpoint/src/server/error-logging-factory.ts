import * as Sentry from '@sentry/node';
import {type Express, type Request, type Response, type NextFunction} from 'express';
import {ValidationError} from '../errors/validation.js';
import {environment, commitSha} from '../config.js';

type ErrorHandler = (
	error: Error,
	request: Request,
	response: Response,
	next: NextFunction
) => void;

type Handler = (
	request: Request,
	response: Response,
	next: NextFunction
) => void;

export type ErrorHandlers = {
	errorHandler: ErrorHandler;
	requestHandler: Handler;
	tracingHandler: Handler;
};

export class ErrorLoggingFactory {
	#sentry: undefined | ErrorHandlers;

	build(app: Express) {
		if (!this.#sentry) {
			this.#sentry = this.#createSentry(app);
		}

		return this.#sentry;
	}

	#createSentry(app: Express) {
		Sentry.init({
			release: commitSha,
			environment: environment === 'production' ? 'PROD' : 'DEV',
			dsn: 'https://b5de903100844604af0761d46be66f0a@o280795.ingest.sentry.io/4505426410733568',
			integrations: [
				// Enable HTTP calls tracing
				new Sentry.Integrations.Http({tracing: true}),
				// Enable Express.js middleware tracing
				new Sentry.Integrations.Express({app}),
				// Automatically instrument Node.js libraries and frameworks
				...Sentry.autoDiscoverNodePerformanceMonitoringIntegrations(),
			],

			// Set tracesSampleRate to 1.0 to capture 100%
			// of transactions for performance monitoring.
			// We recommend adjusting this value in production
			tracesSampleRate: 0.75,
		});

		const errorHandler = Sentry.Handlers.errorHandler({
			shouldHandleError(error) {
				if (error instanceof ValidationError) {
					return false;
				}

				return true;
			},
		});

		return {
			errorHandler,
			requestHandler: Sentry.Handlers.requestHandler(),
			tracingHandler: Sentry.Handlers.tracingHandler(),
		};
	}
}
