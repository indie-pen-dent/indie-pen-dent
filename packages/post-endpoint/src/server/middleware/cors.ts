import {type Request, type Response, type NextFunction} from 'express';
import {environment} from '../../config.js';

export function corsMiddleware(
	request: Request,
	response: Response,
	next: NextFunction,
): void {
	if (environment !== 'production') {
		response.setHeader(
			'Access-Control-Allow-Origin',
			'http://localhost:5173',
		);
		response.setHeader(
			'Access-Control-Allow-Methods',
			'GET, POST, PUT, DELETE, OPTIONS',
		);
		response.setHeader(
			'Access-Control-Allow-Headers',
			'Origin, X-Requested-With, Content-Type, Accept, baggage, sentry-trace, Authorization',
		);
		response.setHeader('Access-Control-Allow-Credentials', 'true');
	}

	if (request.method === 'OPTIONS') {
		// Preflight request
		response.sendStatus(200);
	} else {
		next();
	}
}
