import {type Request, type Response, type NextFunction} from 'express';
import {authManager} from '../../auth/index.js';
import {requestDataStore} from '../request-data.js';
import {pinnerApiKey} from '../../config.js';

export function authenticateUser(
	request: Request,
	response: Response,
	next: NextFunction,
) {
	const token = getToken(request);

	if (!token) {
		return response.status(401).json({error: 'Unauthorized'});
	}

	const userData = authManager.verifyToken(token);

	if (!userData) {
		return response.status(403).json({error: 'Forbidden'});
	}

	requestDataStore.set(request, {user: userData});
	next();
}

export function authenticateApiKey(
	request: Request,
	response: Response,
	next: NextFunction,
) {
	const token = getToken(request);

	if (!token || token !== pinnerApiKey) {
		return response.status(401).json({error: 'Unauthorized'});
	}

	next();
}

function getToken(request: Request): string | undefined {
	const headerToken = request.headers.authorization?.split(' ')[1];
	
	const authCookie: unknown = request.cookies.authorization;

	const cookieToken = isString(authCookie)
		? authCookie.split(' ')[1]
		: undefined;

	return headerToken ?? cookieToken;
}

function isString(value: unknown): value is string {
	return typeof value === 'string';
}
