import {unlink} from 'node:fs/promises';
import * as os from 'node:os';
import * as path from 'node:path';
import multer from 'multer';
import {Router, type Request, type Response, type NextFunction} from 'express';
import {store} from '../../store/index.js';
import {authenticateUser} from '../middleware/auth.js';
import {requestDataStore} from '../request-data.js';
import {hostFactory} from '../../poster/main.js';

const storage = multer.diskStorage({
	destination(_request, _file, callback) {
		callback(null, os.tmpdir());
	},
	filename(_request, file, callback) {
		const timestamp = Date.now();

		const extention = path.extname(file.originalname);
		const originalName = path.basename(file.originalname, extention);
		const cleanName = originalName.replaceAll(/\W/g, '');

		const filename = `${timestamp}-${cleanName}`;

		callback(null, filename);
	},
});

const upload = multer({storage});

export const router: Router = Router();

router.post(
	'/upload',
	authenticateUser,
	upload.single('image'),
	async (request: Request, response: Response, next: NextFunction) => {
		if (!request.file) {
			response.status(400).json({error: 'No file uploaded'});
			return;
		}

		// Access the uploaded file information
		const file = request.file;
		const filePath = file.path;

		const {user} = requestDataStore.get(request);

		const userDetails = await store.users.getById(user.me);

		if (!userDetails) {
			await unlink(file.path);
			next(new Error(`User: ${user.me} not found in database`)); return;
		}

		const host = hostFactory.get(userDetails.gitService);
		host.addSettingsFromStore(userDetails.settings);

		const isUploaded = await host.uploadImage(file.filename, filePath);

		try {
			await unlink(file.path);
		} catch {}

		if (!isUploaded) {
			return response
				.status(500)
				.json({error: 'Internal server error'});
		}

		const imageUrl = new URL(user.me);
		imageUrl.pathname = `/images/${file.filename}`;

		return response.status(201).json({success: true, path: imageUrl});
	},
);
