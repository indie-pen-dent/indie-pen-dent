import {router as authRouter} from './auth.js';
import {router as fileRouter} from './file.js';
import {router as postRouter} from './post.js';
import {router as userRouter} from './user.js';
import {router as profileRouter} from './profile.js';
import {router as blogRouter} from './hosted-blog.js';
import {router as accountRouter} from './account.js';

export const routers = [
	authRouter,
	fileRouter,
	postRouter,
	userRouter,
	profileRouter,
	blogRouter,
	accountRouter,
];
