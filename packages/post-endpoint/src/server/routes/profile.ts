import {Router, type Request, type Response, type NextFunction} from 'express';
import {authenticateUser} from '../middleware/auth.js';
import {requestDataStore} from '../request-data.js';
import {indieWebProfileFetcher} from '../../indie-web/profile-fetcher.js';

export const router: Router = Router();

router.get(
	'/profile',
	authenticateUser,
	async (request: Request, response: Response, next: NextFunction) => {
		const {user} = requestDataStore.get(request);

		try {
			const indieWebProfile = await indieWebProfileFetcher.fetchProfile(
				user.me,
			);

			return response.status(200).json(indieWebProfile);
		} catch (error) {
			console.error('Error gettin user data:', error);
			next(error);
		}
	},
);
