import {Router, type Request, type Response, type NextFunction} from 'express';
import {store} from '../../store/index.js';
import {authenticateUser} from '../middleware/auth.js';
import {requestDataStore} from '../request-data.js';
import {hostFactory} from '../../poster/main.js';
import {indieWebProfileFetcher} from '../../indie-web/profile-fetcher.js';
import {postFormatter} from '../../post/main.js';

export const router: Router = Router();

router.get(
	'/post',
	authenticateUser,
	async (request: Request, response: Response, next: NextFunction) => {
		try {
			const {user} = requestDataStore.get(request);

			const userDetails = await store.users.getById(user.me);

			if (!userDetails) {
				return response
					.status(500)
					.json({error: 'Internal server error'});
			}

			const host = hostFactory.get(userDetails.gitService);
			host.addSettingsFromStore(userDetails.settings);
			
			const posts = await host.getPosts();

			return response.status(200).json({data: posts});
		} catch (error) {
			next(error);

			return;
		}
	},
);

router.get(
	'/post/:name',
	authenticateUser,
	async (request: Request, response: Response, next: NextFunction) => {
		try {
			const {user} = requestDataStore.get(request);

			const userDetails = await store.users.getById(user.me);

			if (!userDetails) {
				return response
					.status(500)
					.json({error: 'Internal server error'});
			}

			const host = hostFactory.get(userDetails.gitService);
			host.addSettingsFromStore(userDetails.settings);
			
			const post = await host.getPost(request.params.name);
			
			return response.status(200).json(post);
		} catch (error) {
			next(error);

			return;
		}
	},
);

router.delete(
	'/post/:name',
	authenticateUser,
	async (request: Request, response: Response, next: NextFunction) => {
		try {
			const {user} = requestDataStore.get(request);

			const userDetails = await store.users.getById(user.me);

			if (!userDetails) {
				return response
					.status(500)
					.json({error: 'Internal server error'});
			}

			const host = hostFactory.get(userDetails.gitService);
			host.addSettingsFromStore(userDetails.settings);
			
			const post = await host.deletePost(request.param.name);
			
			return response.status(200).json(post);
		} catch (error) {
			next(error);

			return;
		}
	},
);

router.put(
	'/post/:name',
	authenticateUser,
	async (request: Request, response: Response, next: NextFunction) => {
		try {
			const data: UnvalidatedPost = {
				content: request.body.content,
				date: request.body.date ?? new Date().toUTCString(),
				tags: request.body.tags ?? '',
				title: request.body.title ?? '',
			};

			if (!validatePost(data)) {
				return response.status(400).json({error: 'Invalid data.'});
			}

			const tags = data.tags.split(',');

			const {user} = requestDataStore.get(request);

			const userDetails = await store.users.getById(user.me);

			if (!userDetails) {
				return response
					.status(500)
					.json({error: 'Internal server error'});
			}

			const profile = await indieWebProfileFetcher.fetchProfile(
				userDetails.id,
			);

			const host = hostFactory.get(userDetails.gitService);
			host.addSettingsFromStore(userDetails.settings);

			const isoDate = new Date().toISOString();

			const meta = {
				author: profile?.name ?? userDetails.id,
				date: data.date,
				tags,
				title: data.title.length > 0 ? data.title : undefined,
			};

			const commitContent = postFormatter.format(meta, data.content);
	
			const cleanPostName = data.title.replaceAll(/\W/g, '');

			const isPosted = await host.replacePost(
				request.params.name,
				`${isoDate}_${cleanPostName}`,
				commitContent,
			);

			if (!isPosted) {
				return response
					.status(500)
					.json({error: 'Internal server error'});
			}

			return response.status(201).json({success: true});
		} catch (error) {
			next(error);
		}
	},
);

router.post(
	'/post',
	authenticateUser,
	async (request: Request, response: Response, next: NextFunction) => {
		try {
			const data: UnvalidatedPost = {
				content: request.body.content,
				date: request.body.date ?? new Date().toUTCString(),
				tags: request.body.tags ?? '',
				title: request.body.title ?? '',
			};

			if (!validatePost(data)) {
				return response.status(400).json({error: 'Invalid data.'});
			}

			const tags = data.tags.split(',');

			const {user} = requestDataStore.get(request);

			const userDetails = await store.users.getById(user.me);

			if (!userDetails) {
				return response
					.status(500)
					.json({error: 'Internal server error'});
			}

			const profile = await indieWebProfileFetcher.fetchProfile(
				userDetails.id,
			);

			const host = hostFactory.get(userDetails.gitService);
			host.addSettingsFromStore(userDetails.settings);

			const isoDate = new Date().toISOString();

			const meta = {
				author: profile?.name ?? userDetails.id,
				date: data.date,
				tags,
				title: data.title.length > 0 ? data.title : undefined,
			};

			const commitContent = postFormatter.format(meta, data.content);

			const cleanPostName = data.title.replaceAll(/\W/g, '');

			const isPosted = await host.uploadPost(
				`${isoDate}_${cleanPostName}`,
				commitContent,
			);

			if (!isPosted) {
				return response
					.status(500)
					.json({error: 'Internal server error'});
			}

			return response.status(201).json({success: true});
		} catch (error) {
			next(error);
		}
	},
);

type UnvalidatedPost = {
	content: unknown;
	date: unknown;
	tags: unknown;
	title: unknown;
};

type ValidatedPost = { 
	content: string;
	date: string;
	tags: string;
	title: string;
};

function validatePost(data: UnvalidatedPost): data is ValidatedPost {
	if (!data.content) {
		return false;
	}

	if (typeof data.content !== 'string') {
		return false;
	}

	if (typeof data.date !== 'string') {
		return false;
	}

	if (new Date(data.date).toString() === 'Invalid Date') {
		return false;
	}

	if (typeof data.tags !== 'string') {
		return false;
	}

	if (typeof data.title !== 'string') {
		return false;
	}

	return true;
}
