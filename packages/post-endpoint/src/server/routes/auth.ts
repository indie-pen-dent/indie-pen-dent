import {randomBytes} from 'node:crypto';
import fetch from 'node-fetch';
import {Router, type Request, type Response, type NextFunction} from 'express';
import {authenticateUser} from '../middleware/auth.js';
import {requestDataStore} from '../request-data.js';
import {responseParser} from '../helpers.js';
import {indieWebAuthClient} from '../../indie-web/auth-client.js';
import {authManager} from '../../auth/index.js';
import {isRecord} from '../../common/guards.js';

export const router: Router = Router();

router.get(
	'/authenticate',
	async (request: Request, response: Response, next: NextFunction) => {
		const webAddress = request.query.webAddress as string;

		if (!webAddress) {
			return response
				.status(400)
				.json({error: 'Web address is required'});
		}

		try {
			const authInformation = await indieWebAuthClient.getAuthInformation(
				webAddress,
			);

			if (!authInformation) {
				return response
					.status(400)
					.json({error: 'Authorization endpoint not found'});
			}

			const state = randomBytes(16).toString('hex');
			const host = request.get('host') ?? '';
			const redirectUri = `${request.protocol}://${host}/api/callback`;
			const clientId = `${request.protocol}://${host}`;

			const authUrl = new URL(authInformation.authEndpoint);
			authUrl.searchParams.append('me', webAddress);
			authUrl.searchParams.append('state', state);
			authUrl.searchParams.append('redirect_uri', redirectUri);
			authUrl.searchParams.append('client_id', clientId);

			response.cookie(
				'indie-auth',
				JSON.stringify({state, webAddress}),
				{
					httpOnly: true,
					expires: new Date(Date.now() + 900_000),
				},
			);

			response.redirect(authUrl.toString());
		} catch (error) {
			console.error('Error authenticating user:', error);
			next(error);
		}
	},
);

router.get(
	'/callback',
	async (request: Request, response: Response, next: NextFunction) => {
		const code = request.query.code as string;
		const state = request.query.state as string;
		const authCookie: unknown = request.cookies['indie-auth'];

		if (!code || !state || !authCookie) {
			return response
				.status(400)
				.json({error: 'Invalid callback parameters'});
		}

		try {
			if (typeof authCookie !== 'string') {
				return response.status(403).send();
			}
			
			const cookieData: unknown = JSON.parse(authCookie);
			if (!isRecord(cookieData) || !('state' in cookieData) || !('webAddress' in cookieData)) {
				return response.status(400).json({
					error: 'No user with that web address attempting to login',
				});
			}

			if (state !== cookieData.state) {
				return response.status(401).json({
					error: 'Returned state does not match',
				});
			}

			if (typeof cookieData.webAddress !== 'string') {
				return response.status(400).json({
					error: 'webAddess is of the wrong type',
				});
			}

			const authInformation = await indieWebAuthClient.getAuthInformation(
				cookieData.webAddress,
			);

			if (!authInformation) {
				return response
					.status(400)
					.json({error: 'Token endpoint not found'});
			}

			const host = request.get('host') ?? '';

			const redirectUri = `${request.protocol}://${host}/api/callback`;

			const queryParameters = new URLSearchParams();
			queryParameters.append('code', code);
			queryParameters.append('redirect_uri', redirectUri);
			queryParameters.append(
				'client_id',
				`${request.protocol}://${host}`,
			);

			const authResponse = await fetch(authInformation.authEndpoint, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
				},
				body: queryParameters.toString(),
			});

			if (!authResponse.ok) {
				return response
					.status(400)
					.json({error: 'Failed to retrieve access token'});
			}

			const {me} = (await responseParser(authResponse)) as {
				me: string;
			};

			const token = authManager.generateToken({me});

			response.cookie('authorization', `bearer ${token}`, {
				sameSite: 'lax',
			});

			response.redirect('/app/auth_passed');
		} catch (error) {
			console.error('Error authenticating user:', error);
			next(error);
		}
	},
);

router.get(
	'/token',
	authenticateUser,
	(request: Request, response: Response) => {
		const authCookie: unknown = request.cookies.authorization;

		const {user} = requestDataStore.get(request);

		if (!authCookie || typeof authCookie !== 'string') {
			return response.status(401).json({error: 'Unauthenticated'});
		}

		const [, token] = authCookie.split(' ');

		return response.status(200).json({
			token,
			me: user.me,
		});
	},
);
