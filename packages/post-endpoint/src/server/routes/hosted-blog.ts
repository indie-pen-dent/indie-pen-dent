import {Router, type Request, type Response, type NextFunction} from 'express';
import {store} from '../../store/index.js';
import {authenticateApiKey} from '../middleware/auth.js';

export const router: Router = Router();

router.get(
	'/hosted-blogs/:blogName',
	authenticateApiKey,
	async (request: Request, response: Response, next: NextFunction) => {
		try {
			const blogData = await store.hostedBlogs.getById(
				request.params.blogName,
			);

			if (!blogData) {
				return response.status(404).end();
			}

			return response.status(200).json(blogData);
		} catch (error) {
			console.error('Error getting blog data:', error);
			next(error);
		}
	},
);

router.put(
	'/hosted-blogs/:blogName',
	authenticateApiKey,
	async (request: Request, response: Response, next: NextFunction) => {
		const pinnedHash: unknown = request.body.pinnedHash;
		if (typeof pinnedHash !== 'string') {
			return response.status(400).json({
				error: 'Missing pinnedHash is body.',
			});
		}

		try {
			const updatedBlogData = await store.hostedBlogs.update(
				request.params.blogName,
				{
					id: request.params.blogName,
					pinnedHash: pinnedHash,
				},
			);

			if (!updatedBlogData) {
				return response.status(404).end();
			}

			return response.status(200).json({success: true});
		} catch (error) {
			console.error('Error getting blog data:', error);
			next(error);
		}
	},
);
