import process from 'node:process';
import {Router, type Request, type Response, type NextFunction} from 'express';
import {Container} from '../../container.js';
import {ValidationError} from '../../errors/validation.js';
import {verifyContainer} from '../../verify/verify-container.js';
import {DnsContainer} from '../../dns/dns-container.js';
import {store} from '../../store/index.js';
import {hostFactory} from '../../poster/main.js';

const container = new Container(process.env);

const skribiloService = container.getSkribiloHost();
const mailer = container.getMailer();

type UnValidatedRequestData = {
	email: unknown;
	blogName: unknown;
};

type PostRequestData = {
	email: string;
	blogName: string;
};

type VerifyRequestData = {
	email: string;
	code: string;
};

type UnValidatedVerifyRequestData = {
	email: unknown;
	code: unknown;
};

export const router: Router = Router();

router.post(
	'/account',
	async (request: Request, response: Response, next: NextFunction) => {
		const data: UnValidatedRequestData = {
			email: request.body.email,
			blogName: request.body.blogName,
		};

		try {
			if (!validatePostRequest(data)) {
				throw new Error('Not possible.');
			}
		} catch (error) {
			next(error);
			return;
		}

		const existingBlog = await store.skribiloBlogs.getById(data.blogName);

		if (existingBlog) {
			next(new ValidationError('Blog name already taken.', [])); return;
		}

		const verifier = verifyContainer.getVerifier();

		const verificationCode = verifier.startVerification(
			data.email,
			data.blogName,
		);

		const text = `Your Skribilo verification code: ${verificationCode}`;

		try {
			await mailer.sendEmail({
				to: data.email,
				from: 'auth@skribilo.blog',
				subject: 'Skribilo Verification Email',
				text,
			});
		} catch (error) {
			next(error);
			return;
		}

		response.status(200).redirect('/sign-up/verify');
	},
);

router.post(
	'/account/verify',
	async (request: Request, response: Response, next: NextFunction) => {
		const data: UnValidatedVerifyRequestData = {
			email: request.body.email,
			code: request.body.code,
		};

		try {
			if (!validateVerifyRequest(data)) {
				throw new Error('Not possible.');
			}
		} catch (error) {
			next(error);
			return;
		}

		const verifier = verifyContainer.getVerifier();

		const blogName = verifier.verifyCode(
			data.email,
			data.code,
		);

		if (!blogName) {
			return response.status(403).end();
		}

		try {
			const dnsContainer = new DnsContainer();
			const dnsProvider = dnsContainer.getDns();

			await store.skribiloBlogs.create({id: blogName});
			const hostId = await dnsProvider.create(blogName);

			await skribiloService.setUpHosting(
				blogName,
				data.email,
				hostId,
			);

			const host = hostFactory.get('Skribilo');
			host.addSettingsFromClient({
				apiUrl: skribiloService.config.apiUrl,
				apiKey: skribiloService.config.apiToken,
				owner: skribiloService.config.owner,
				repositoryId: blogName,
				branchName: 'master',
			});

			const settings = host.getSettingsForStore();

			await store.users.create({
				id: `https://${blogName}.skribilo.blog/`,
				gitService: 'Skribilo',
				settings,
			});
		} catch (error) {
			next(error); return;
		}

		response.status(200).redirect('/sign-up/complete');
	},
);

function validatePostRequest(
	data: UnValidatedRequestData,
): data is PostRequestData {
	const errors = [];

	if (typeof data.email !== 'string' || data.email.length === 0) {
		errors.push('Email must be a string.');
	}

	if (typeof data.blogName !== 'string' || data.blogName.length === 0) {
		errors.push('BlogName must be a string.');
	}

	if (typeof data.blogName === 'string' && /[^a-z]/g.test(data.blogName)) {
		errors.push('BlogName must only contain characters a - z.');
	}

	if (errors.length > 0) {
		throw new ValidationError('Invalid input', errors);
	}

	return true;
}

function validateVerifyRequest(
	data: UnValidatedVerifyRequestData,
): data is VerifyRequestData {
	const errors = [];

	if (typeof data.email !== 'string' || data.email.length === 0) {
		errors.push('Email must be a string.');
	}

	if (typeof data.code !== 'string' || data.code.length === 0) {
		errors.push('Code must be a string.');
	}

	if (errors.length > 0) {
		throw new ValidationError('Invalid input', errors);
	}

	return true;
}
