import {Router, type Request, type Response, type NextFunction} from 'express';
import {store} from '../../store/index.js';
import {authenticateUser} from '../middleware/auth.js';
import {requestDataStore} from '../request-data.js';
import {hostFactory} from '../../poster/main.js';
import {isRecord} from '../../common/guards.js';

export const router: Router = Router();

router.get(
	'/user',
	authenticateUser,
	async (request: Request, response: Response, next: NextFunction) => {
		const {user} = requestDataStore.get(request);

		try {
			const userData = await store.users.getById(user.me);

			if (userData === undefined) {
				return response.status(404).json({
					error: 'No Account',
				});
			}

			const host = hostFactory.get(userData.gitService);
			host.addSettingsFromStore(userData.settings);

			const settings = host.getSettingsForClient();

			return response.status(200).json({
				gitService: userData.gitService,
				settings,
			});
		} catch (error) {
			console.error('Error gettin user data:', error);
			next(error);
		}
	},
);

router.post(
	'/user',
	authenticateUser,
	async (request: Request, response: Response, next: NextFunction) => {
		const requestBody = await validateRequest(request);

		if (requestBody instanceof Error) {
			return response.status(400).json({error: requestBody.message});
		}

		try {
			const {user} = requestDataStore.get(request);

			const host = hostFactory.get(requestBody.gitService);
			host.addSettingsFromClient(requestBody.settings);

			const settings = host.getSettingsForStore();

			await store.users.create({
				id: user.me,
				gitService: requestBody.gitService,
				settings,
			});

			return response.status(201).json({success: true});
		} catch (error) {
			console.error('Error storing API key:', error);
			next(error);
		}
	},
);

router.put(
	'/user',
	authenticateUser,
	async (request: Request, response: Response, next: NextFunction) => {
		const requestBody = await validateRequest(request);

		if (requestBody instanceof Error) {
			return response.status(400).json({error: requestBody.message});
		}

		try {
			const {user} = requestDataStore.get(request);

			const host = hostFactory.get(requestBody.gitService);
			host.addSettingsFromClient(requestBody.settings);

			const settings = host.getSettingsForStore();

			await store.users.update(user.me, {
				gitService: requestBody.gitService,
				settings,
			});

			return response.status(201).json({success: true});
		} catch (error) {
			console.error('Error storing API key:', error);
			next(error);
		}
	},
);

type RequestBody = {
	gitService: string;
	settings: Record<string, unknown>;
};

async function validateRequest(request: Request): Promise<RequestBody | Error> {
	const gitService: unknown = request.body.gitService;

	if (!gitService || typeof gitService !== 'string') {
		return new Error('Git Service is required');
	}

	const settings: unknown = request.body.settings;

	if (!settings || !isRecord(settings)) {
		return new Error('A settings object is required');
	}

	return {
		gitService,
		settings,
	};
}
