import {type Request} from 'express';

type RequestContext = {
	user: {
		me: string;
	};
};

export class RequestDataStore {
	#data = new WeakMap<Request, RequestContext>();

	set(request: Request, data: RequestContext) {
		this.#data.set(request, data);
	}

	get(request: Request): RequestContext {
		const data = this.#data.get(request);

		if (!data) {
			throw new Error('Request Data not found');
		}

		return data;
	}
}

export const requestDataStore = new RequestDataStore();
