import express, {
	type Express,
	type Router,
	type Request,
	type Response,
	type NextFunction,
} from 'express';
import cookieParser from 'cookie-parser';
import type {InErrorHandler} from '../types/errors.js';
import {corsMiddleware} from './middleware/cors.js';
import {type ErrorHandlers} from './error-logging-factory.js';

export class ExpressServer {
	private readonly app: Express;
	private readonly port: number;
	private readonly routes: Router[];
	private readonly baseUrl: string;
	private readonly errorLoggers: ErrorHandlers;
	#errorHandler: InErrorHandler;

	constructor(
		port: number,
		baseUrl: string,
		routes: Router[],
		errorLoggerFactory: {build: (app: Express) => ErrorHandlers},
		errorHandler: InErrorHandler,
	) {
		this.app = express();
		this.port = port;
		this.routes = routes;
		this.baseUrl = baseUrl;
		this.errorLoggers = errorLoggerFactory.build(this.app);
		this.#errorHandler = errorHandler;
	}

	public start(): void {
		this.setupMiddleware();
		this.setupRoutes();
		this.setupErrorRoute();

		this.app.listen(this.port, () => {
			console.log(`Server is running on port ${this.port}`);
		});
	}

	private setupMiddleware(): void {
		this.app.use(corsMiddleware);
		this.app.use(express.json());
		this.app.use(express.urlencoded({extended: true}));
		this.app.use(cookieParser());
		this.app.use(this.errorLoggers.requestHandler);
		this.app.use(this.errorLoggers.tracingHandler);
	}

	private setupRoutes(): void {
		for (const route of this.routes) {
			this.app.use(this.baseUrl, route);
		}
	}

	private setupErrorRoute(): void {
		this.app.use(this.errorLoggers.errorHandler);

		this.app.use(
			(
				error: Error,
				_request: Request,
				response: Response,
				_next: NextFunction,
			) => {
				this.#errorHandler.handle(error);
				const sanitisedError = this.#errorHandler.sanitise(error);

				const errorString = Buffer.from(
					JSON.stringify(sanitisedError),
				).toString('base64');

				const url = `/error?details=${errorString}`;

				response.status(sanitisedError.status).redirect(url);
			},
		);
	}
}
