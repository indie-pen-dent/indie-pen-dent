import {port, baseUrl} from '../config.js';
import {Container} from '../container.js';
import {routers} from './routes/index.js';
import {ExpressServer} from './server.js';
import {ErrorLoggingFactory} from './error-logging-factory.js';

const container = new Container(process.env);

const errorHandler = container.getErrorHandler();

const errorloggers = new ErrorLoggingFactory();

const server = new ExpressServer(
	port,
	baseUrl,
	routers,
	errorloggers,
	errorHandler,
);

server.start();
