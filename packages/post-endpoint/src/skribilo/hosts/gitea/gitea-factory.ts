import {GiteaConfig} from './gitea-config.js';
import {GiteaRepositoryCloner} from './gitea.js';

export class GiteaFactory {
	create(environment: Record<string, unknown>) {
		const giteaConfig = new GiteaConfig(environment);

		return new GiteaRepositoryCloner(giteaConfig);
	}
}
