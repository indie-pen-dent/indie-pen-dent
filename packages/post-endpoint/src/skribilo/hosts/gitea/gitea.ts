import fetch, {Headers} from 'node-fetch';
import {ThirdPartyError} from '../../../errors/third-party.js';
import {type SkribiloHost} from '../../types.js';

type Config = {
	apiUrl: string;
	apiToken: string;
	owner: string;
};

export class GiteaRepositoryCloner implements SkribiloHost {
	readonly config: Config;
	private readonly apiUrl: string;
	private readonly apiToken: string;
	private readonly owner: string;
	private readonly templateRepo: string;

	constructor(config: {
		apiUrl: string;
		apiToken: string;
		owner: string;
		templateRepo: string;
	}) {
		this.apiUrl = `${config.apiUrl}/api/v1`;
		this.apiToken = config.apiToken;
		this.owner = config.owner;
		this.templateRepo = config.templateRepo;

		this.config = {
			apiUrl: config.apiUrl,
			apiToken: config.apiToken,
			owner: config.owner,
		};
	}

	async cloneRepository(
		newRepoName: string,
		newRepoDescription: string,
	): Promise<void> {
		const url = `${this.apiUrl}/repos/${this.owner}/${this.templateRepo}/generate`;
		const headers = this.getHeaders();
		const data = {
			name: newRepoName,
			description: newRepoDescription,
			owner: this.owner,
			// eslint-disable-next-line @typescript-eslint/naming-convention
			git_content: true,
			private: true, // Set this to true if you want a private repository
		};

		const response = await fetch(url, {
			method: 'POST',
			headers,
			body: JSON.stringify(data),
		});

		if (!response.ok) {
			const details = {
				name: 'gitea',
				url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Gitea error cloning repository',
				details,
			);
		}
	}

	private getHeaders(): Headers {
		const headers = new Headers();
		headers.set('Authorization', `token ${this.apiToken}`);
		headers.set('Content-Type', 'application/json');
		return headers;
	}
}
