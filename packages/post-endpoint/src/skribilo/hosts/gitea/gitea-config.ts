type GiteaEnvironment = {
	apiUrl: string;
	apiToken: string;
	owner: string;
	templateRepo: string;
};

export class GiteaConfig {
	readonly apiUrl: string;
	readonly apiToken: string;
	readonly owner: string;
	readonly templateRepo: string;

	constructor(environment: Record<string, unknown>) {
		const giteaEnvironment = this.#validate(environment);

		this.apiUrl = giteaEnvironment.apiUrl;
		this.apiToken = giteaEnvironment.apiToken;
		this.owner = giteaEnvironment.owner;
		this.templateRepo = giteaEnvironment.templateRepo;
	}

	#validate(environment: Record<string, unknown>): GiteaEnvironment {
		const result: Partial<GiteaEnvironment> = {};

		if (!this.#validateUrl(environment.GITEA_URL)) {
			throw new Error('Invalid GITEA_URL');
		}

		result.apiUrl = environment.GITEA_URL;

		if (!this.#validateToken(environment.GITEA_TOKEN)) {
			throw new Error('Invalid GITEA_TOKEN');
		}

		result.apiToken = environment.GITEA_TOKEN;

		if (!this.#validateOwner(environment.GITEA_OWNER)) {
			throw new Error('Invalid GITEA_OWNER');
		}

		result.owner = environment.GITEA_OWNER;

		if (!this.#validateTemplateRepo(environment.GITEA_TEMPLATE_REPO)) {
			throw new Error('Invalid GITEA_TEMPLATE_REPO');
		}

		result.templateRepo = environment.GITEA_TEMPLATE_REPO;

		return result as GiteaEnvironment;
	}

	#validateUrl(url: unknown): url is string {
		if (typeof url !== 'string') {
			return false;
		}

		try {
			new URL(url);
			return true;
		} catch {
			return false;
		}
	}

	#validateOwner(owner: unknown): owner is string {
		return typeof owner === 'string';
	}

	#validateToken(token: unknown): token is string {
		return typeof token === 'string';
	}

	#validateTemplateRepo(token: unknown): token is string {
		return typeof token === 'string';
	}
}
