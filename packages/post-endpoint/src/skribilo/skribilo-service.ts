import {type SkribiloHost, type SkribiloDeployer} from './types.js';

type Config = {
	apiUrl: string;
	apiToken: string;
	owner: string;
};

export class SkribiloService {
	readonly config: Config;
	#host: SkribiloHost;
	#deployer: SkribiloDeployer;

	constructor(
		host: SkribiloHost,
		deployer: SkribiloDeployer,
		config: Config,
	) {
		this.#host = host;
		this.#deployer = deployer;
		this.config = config;
	}

	async setUpHosting(
		blogName: string,
		description: string,
		hostId: string,
	): Promise<void> {
		await this.#host.cloneRepository(blogName, description);

		await this.#deployer.setupDeploy(blogName, hostId);
	}
}
