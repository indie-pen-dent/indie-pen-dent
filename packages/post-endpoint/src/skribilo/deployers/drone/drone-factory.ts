import {DroneConfig} from './drone-config.js';
import {DroneApiClient} from './drone.js';

export class DroneFactory {
	create(environment: Record<string, unknown>): DroneApiClient {
		const droneConfig = new DroneConfig(environment);

		return new DroneApiClient(droneConfig);
	}
}
