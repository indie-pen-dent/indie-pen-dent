type DroneEnvironment = {
	url: string;
	token: string;
	owner: string;
};

export class DroneConfig {
	readonly url: string;
	readonly token: string;
	readonly owner: string;

	constructor(environment: Record<string, unknown>) {
		const droneEnvironment = this.#validate(environment);

		this.url = droneEnvironment.url;
		this.token = droneEnvironment.token;
		this.owner = droneEnvironment.owner;
	}

	#validate(environment: Record<string, unknown>): DroneEnvironment {
		const result: Partial<DroneEnvironment> = {};

		if (!this.#validateUrl(environment.DRONE_URL)) {
			throw new Error('Invalid DRONE_URL');
		}

		result.url = environment.DRONE_URL;

		if (!this.#validateToken(environment.DRONE_TOKEN)) {
			throw new Error('Invalid DRONE_TOKEN');
		}

		result.token = environment.DRONE_TOKEN;

		if (!this.#validateOwner(environment.DRONE_OWNER)) {
			throw new Error('Invalid DRONE_OWNER');
		}

		result.owner = environment.DRONE_OWNER;

		return result as DroneEnvironment;
	}

	#validateUrl(url: unknown): url is string {
		if (typeof url !== 'string') {
			return false;
		}

		try {
			new URL(url);
			return true;
		} catch {
			return false;
		}
	}

	#validateOwner(owner: unknown): owner is string {
		return typeof owner === 'string';
	}

	#validateToken(token: unknown): token is string {
		return typeof token === 'string';
	}
}
