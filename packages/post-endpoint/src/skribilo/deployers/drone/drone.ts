import fetch, {type Response} from 'node-fetch';
import {ThirdPartyError} from '../../../errors/third-party.js';

export class DroneApiClient {
	private readonly baseUrl: string;
	private readonly authToken: string;
	private readonly owner: string;

	constructor(config: {url: string; token: string; owner: string}) {
		this.baseUrl = config.url;
		this.authToken = config.token;
		this.owner = config.owner;
	}

	async setupDeploy(blogName: string, hostId: string): Promise<void> {
		await this.syncRepositories();

		await this.activateRepository(blogName);

		await this.setRepositorySecret(blogName, 'BLOG_NAME', blogName);
		await this.setRepositorySecret(
			blogName,
			'CLOUDFLARE_HOSTNAME_ID',
			hostId,
		);

		await this.triggerBuild(blogName);
	}

	private async sendRequest(
		path: string,
		method: string,
		body?: Record<string, string | boolean>,
	): Promise<Response> {
		const url = `${this.baseUrl}${path}`;
		const headers = {
			// eslint-disable-next-line @typescript-eslint/naming-convention
			Authorization: `Bearer ${this.authToken}`,
			'Content-Type': 'application/json',
		};

		return fetch(url, {
			method,
			headers,
			body: body ? JSON.stringify(body) : undefined,
		});
	}

	private async syncRepositories(): Promise<void> {
		const response = await this.sendRequest('/api/user/repos', 'POST');

		if (!response.ok) {
			const details = {
				name: 'drone',
				url: response.url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Drone: none OK status syncing repositories.',
				details,
			);
		}
	}

	private async activateRepository(name: string): Promise<void> {
		const response = await this.sendRequest(
			`/api/repos/${this.owner}/${name}`,
			'POST',
		);

		if (!response.ok) {
			const details = {
				name: 'drone',
				url: response.url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Drone: none OK status activating repository.',
				details,
			);
		}
	}

	private async setRepositorySecret(
		name: string,
		secretName: string,
		secretValue: string,
	): Promise<void> {
		const response = await this.sendRequest(
			`/api/repos/${this.owner}/${name}/secrets`,
			'POST',
			{
				name: secretName,
				data: secretValue,
				// eslint-disable-next-line @typescript-eslint/naming-convention
				pull_request: false,
			},
		);

		if (!response.ok) {
			const details = {
				name: 'drone',
				url: response.url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Drone: none OK status setting secret.',
				details,
			);
		}
	}

	private async triggerBuild(name: string): Promise<void> {
		const response = await this.sendRequest(
			`/api/repos/${this.owner}/${name}/builds`,
			'POST',
		);

		if (!response.ok) {
			const details = {
				name: 'drone',
				url: response.url,
				status: response.status,
				responseBody: await response.text(),
			};

			throw new ThirdPartyError(
				'Drone: none OK status setting triggering build.',
				details,
			);
		}
	}
}
