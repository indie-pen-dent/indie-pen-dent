import {GiteaFactory} from './hosts/gitea/gitea-factory.js';
import {DroneFactory} from './deployers/drone/drone-factory.js';
import {type SkribiloHost, type SkribiloDeployer} from './types.js';
import {SkribiloService} from './skribilo-service.js';

export class SkribiloHostContainer {
	#environment: Record<string, unknown>;

	constructor(environment: Record<string, unknown>) {
		this.#environment = environment;
	}

	getSkribiloService() {
		const host = this.getHost();
		const deployer = this.getDeployer();

		return new SkribiloService(host, deployer, host.config);
	}

	getHost(): SkribiloHost {
		const hosts = this.#getHostsObject();

		const hostType = this.#environment.SKRIBILO_HOST_TYPE;

		const requestedHost
            = typeof hostType === 'string' ? hosts[hostType] : undefined;

		if (!requestedHost) {
			throw new Error(`Unknown host ${String(hostType)}.`);
		}

		return requestedHost();
	}

	getDeployer(): SkribiloDeployer {
		const deployers = this.#getDeployerObject();

		const deployer = this.#environment.SKRIBILO_DEPLOYER;

		const deployerFactory
            = typeof deployer === 'string' ? deployers[deployer] : undefined;

		if (!deployerFactory) {
			throw new Error(`Unknown deployer ${String(deployer)}.`);
		}

		return deployerFactory();
	}

	#getHostsObject(): Record<string, () => SkribiloHost> {
		return {
			gitea: () => new GiteaFactory().create(this.#environment),
		};
	}

	#getDeployerObject(): Record<string, () => SkribiloDeployer> {
		return {
			drone: () => new DroneFactory().create(this.#environment),
		};
	}
}
