export type SkribiloHost = {
	config: {
		apiUrl: string;
		apiToken: string;
		owner: string;
	};
	cloneRepository: (
		newRepoName: string,
		newRepoDescription: string
	) => Promise<void>;
};

export type SkribiloDeployer = {
	setupDeploy: (blogName: string, hostId: string) => Promise<void>;
};
