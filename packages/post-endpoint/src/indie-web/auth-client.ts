import fetch from 'node-fetch';
import {JSDOM} from 'jsdom';

type IndieWebAuthResponse = {
	me: string;
	authEndpoint: string;
};

export class IndieWebAuthClient {
	async getAuthInformation(
		url: string,
	): Promise<IndieWebAuthResponse | undefined> {
		const response = await fetch(url);
		const html = await response.text();

		const {window} = new JSDOM(html);
		const document = window.document;

		const authEndpoint = this.findAuthEndpoint(document);
		const me = this.findMeUrl(document);

		if (authEndpoint && me) {
			return {authEndpoint, me};
		}
	}

	private findAuthEndpoint(parsedHtml: Document): string | undefined {
		const link = parsedHtml.querySelector(
			'link[rel="authorization_endpoint"]',
		);

		const href = link ? link.getAttribute('href') : undefined;

		return href ?? undefined;
	}

	private findMeUrl(parsedHtml: Document): string | undefined {
		const link = parsedHtml.querySelector('link[rel="me"]');

		const href = link ? link.getAttribute('href') : undefined;

		return href ?? undefined;
	}

	private findTokenEndpoint(parsedHtml: Document): string | undefined {
		const link = parsedHtml.querySelector('link[rel="token_endpoint"]');

		const href = link ? link.getAttribute('href') : undefined;

		return href ?? undefined;
	}
}

export const indieWebAuthClient = new IndieWebAuthClient();
