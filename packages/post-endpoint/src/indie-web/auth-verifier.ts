import fetch from 'node-fetch';

type IndieWebAuthTokenVerificationResponse = {
	me: string;
	scope?: string;
	client_id?: string;
	issued_at?: number;
};

export class IndieWebAuthVerifier {
	async verifyAuthToken(
		authEndpoint: string,
		authToken: string,
	): Promise<IndieWebAuthTokenVerificationResponse | undefined> {
		const response = await fetch(authEndpoint, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
			body: `code=${encodeURIComponent(authToken)}`,
		});

		if (!response.ok) {
			throw new Error(
				`Error verifying IndieWeb auth token: ${await response.text()}`,
			);
		}

		const verificationResponse = await response.json();

		return verificationResponse as IndieWebAuthTokenVerificationResponse;
	}
}
