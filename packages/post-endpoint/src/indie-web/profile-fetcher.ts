import fetch from 'node-fetch';
import {JSDOM} from 'jsdom';

type IndieWebProfile = {
	name: string;
	photoUrl: string;
	website: string;
	bio: string;
};

export class IndieWebProfileFetcher {
	async fetchProfile(
		webAddress: string,
	): Promise<IndieWebProfile | undefined> {
		try {
			const response = await fetch(webAddress);
			const html = await response.text();
			const {window} = new JSDOM(html);
			const document = window.document;

			const profile: IndieWebProfile = {
				name: this.extractMetaContent(document, 'name') ?? '',
				photoUrl: this.extractMetaContent(document, 'photo') ?? '',
				website: webAddress,
				bio: this.extractMetaContent(document, 'note') ?? '',
			};

			return profile;
		} catch (error) {
			console.error('Error fetching IndieWeb profile:', error);
		}
	}

	private extractMetaContent(
		document: Document,
		property: string,
	): string | undefined {
		const element = document.querySelector(
			`.p-${property}`,
		)!;

		return element?.textContent ? element.textContent.trim() : undefined;
	}
}

export const indieWebProfileFetcher = new IndieWebProfileFetcher();
