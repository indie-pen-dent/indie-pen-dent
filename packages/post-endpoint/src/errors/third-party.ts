type InThirdPartyDetails = {
	name: string;
	url: string;
	status: number;
	responseBody: string;
};

export class ThirdPartyError extends Error {
	readonly details: InThirdPartyDetails;

	constructor(message: string, details: InThirdPartyDetails) {
		super(message);

		this.details = details;
	}
}
