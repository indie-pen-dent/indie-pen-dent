import type {InErrorHandler} from '../types/errors.js';
import {ValidationError} from './validation.js';
import {ThirdPartyError} from './third-party.js';

type ErrorReporter = {
	captureException: (error: Error, details?: Record<string, unknown>) => void;
};

export class ErrorHandler implements InErrorHandler {
	#errorReporter: ErrorReporter;

	constructor(errorReporter: ErrorReporter) {
		this.#errorReporter = errorReporter;
	}

	handle(error: Error) {
		if (error instanceof ValidationError) {
			return;
		}

		if (error instanceof ThirdPartyError) {
			console.log('ERROR:', JSON.stringify(error.details, undefined, 4));
			this.#errorReporter.captureException(error, {
				contexts: {
					details: error.details,
				},
			});
		}

		console.log('UNKNOWN ERROR:', error);
		this.#errorReporter.captureException(error);
	}

	sanitise(error: Error) {
		if (error instanceof ValidationError) {
			return {
				status: 400,
				name: 'Validation Error',
				error: error.message,
				details: error.details,
			};
		}

		return {
			status: 500,
			name: 'Internal Server Error',
			error: 'Internal Server Error',
			details: [],
		};
	}
}
