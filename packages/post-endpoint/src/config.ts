import process from 'node:process';
// eslint-disable-next-line import/no-unassigned-import
import 'dotenv/config.js';

const defaults = {
	port: 3000,
	baseUrl: '',
	commitSha: 'DEV',
};

if (process.env.JWT_KEY === undefined) {
	throw new Error('A JWT_KEY is required');
}

if (process.env.PORT === undefined) {
	console.warn(`No PORT environment variable set, using ${defaults.port}`);
}

if (process.env.DATABASE_URL === undefined) {
	throw new Error('A DATABASE_URL is required');
}

if (!process.env.BASE_URL) {
	console.warn(
		`No BASE_URL environment variable set, using ${defaults.baseUrl}`,
	);
}

if (process.env.NODE_ENV === 'development') {
	console.warn('Running in development mode');
} else {
	console.warn('Running in production mode');
}

if (!process.env.COMMIT_SHA) {
	console.warn('No COMMIT_SHA, errors will not be grouped by a release');
}

if (!process.env.PINNER_API_KEY) {
	throw new Error('A PINNER_API_KEY environment variable is required.');
}

if (!process.env.DNS_PROVIDER) {
	throw new Error('A DNS_PROVIDER environment variable is required.');
}

if (!process.env.CLOUDFLARE_API_KEY) {
	throw new Error('A CLOUDFLARE_API_KEY environment variable is required.');
}

if (!process.env.CLOUDFLARE_ZONE_ID) {
	throw new Error('A CLOUDFLARE_ZONE_ID environment variable is required.');
}

if (!process.env.CLOUDFLARE_EMAIL) {
	throw new Error('A CLOUDFLARE_EMAIL environment variable is required.');
}

export const jwtSecret = process.env.JWT_KEY;
export const port = Number(process.env.port ?? defaults.port);
export const databaseUrl = process.env.DATABASE_URL;
export const baseUrl = process.env.BASE_URL ?? defaults.baseUrl;
export const environment = process.env.NODE_ENV ?? 'production';
export const commitSha = process.env.COMMIT_SHA ?? defaults.commitSha;
export const pinnerApiKey = process.env.PINNER_API_KEY;

export const dnsProvider = process.env.DNS_PROVIDER;

export const cloudflare = {
	apiToken: process.env.CLOUDFLARE_API_KEY,
	zoneId: process.env.CLOUDFLARE_ZONE_ID,
	apiEmail: process.env.CLOUDFLARE_EMAIL,
};
