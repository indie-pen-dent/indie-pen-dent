import * as Sentry from '@sentry/node';
import {ErrorHandler} from './errors/error-handler.js';
import {MailerContainer} from './mailer/mailer-container.js';
import {SkribiloHostContainer} from './skribilo/skribilo-host-container.js';
import {SchemaValidatorFactory} from './schema-validator/schema-validator.js';
// Hosts
import {HostFactory} from './poster/host-factory.js';
import {hostHandlers} from './poster/hosts.js';

export class Container {
	#environment: Record<string, unknown>;

	constructor(environment: Record<string, unknown>) {
		this.#environment = environment;
	}

	getMailer() {
		return new MailerContainer(this.#environment).getMailer();
	}

	getSkribiloHost() {
		return new SkribiloHostContainer(
			this.#environment,
		).getSkribiloService();
	}

	getErrorHandler() {
		return new ErrorHandler(Sentry);
	}

	getSchemaValidatorFactory() {
		return new SchemaValidatorFactory();
	}

	getHostFactory() {
		return new HostFactory(hostHandlers);
	}
}
