import fetch from 'node-fetch';

type GitLabTokenScope = {
	api: boolean;
	read_user: boolean;
	read_repository: boolean;
	write_repository: boolean;
};

export class GitLabTokenScopeChecker {
	get apiUrl() {
		return 'https://gitlab.com/api/v4';
	}

	async checkTokenScope(
		token: string,
	): Promise<GitLabTokenScope | undefined> {
		try {
			const response = await fetch(this.apiUrl, {
				headers: {
					'Content-Type': 'application/json',
					'PRIVATE-TOKEN': token,
				},
			});

			if (response.ok) {
				const userData = await response.json();
				const scopes = this.extractTokenScopes(
					(userData as Record<string, string>).scopes,
				);
				return scopes;
			}

			console.error(
				'Error checking token scope:',
				response.statusText,
			);
		} catch (error) {
			console.error('Error checking token scope:', error);
		}
	}

	async hasAccessToAllRepositories(
		token: string,
	): Promise<boolean | undefined> {
		try {
			const response = await fetch(`${this.apiUrl}/projects`, {
				headers: {
					'Content-Type': 'application/json',
					'PRIVATE-TOKEN': token,
				},
			});

			if (response.ok) {
				const projectsData = await response.json();
				return (
					(Array.isArray(projectsData) ? projectsData : []).length > 1
				);
			}

			console.error(
				'Error checking access to all repositories:',
				response.statusText,
			);
		} catch (error) {
			console.error('Error checking access to all repositories:', error);
		}
	}

	private extractTokenScopes(scopes: string): GitLabTokenScope {
		const parsedScopes: GitLabTokenScope = {
			api: false,
			// eslint-disable-next-line @typescript-eslint/naming-convention
			read_user: false,
			// eslint-disable-next-line @typescript-eslint/naming-convention
			read_repository: false,
			// eslint-disable-next-line @typescript-eslint/naming-convention
			write_repository: false,
		};

		const scopeArray = scopes.split(',');

		for (const scope of scopeArray) {
			switch (scope) {
				case 'api': {
					parsedScopes.api = true;
					break;
				}

				case 'read_user': {
					parsedScopes.read_user = true;
					break;
				}

				case 'read_repository': {
					parsedScopes.read_repository = true;
					break;
				}

				case 'write_repository': {
					parsedScopes.write_repository = true;
					break;
				}
			}
		}

		return parsedScopes;
	}
}

export const gitLabTokenScopeChecker = new GitLabTokenScopeChecker();
