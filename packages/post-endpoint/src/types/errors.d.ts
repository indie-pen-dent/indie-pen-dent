export type InErrorHandler = {
	handle: (error: Error) => void;
	sanitise: (error: Error) => {
		status: number;
		name: string;
		error: string;
		details: string[];
	};
};
