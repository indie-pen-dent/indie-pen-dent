type ImageUrlExtractor = {
	extract: (content: string) => string[];
};

type PostMetaData = {
	author: string;
	date: string;
	tags: string[];
	title?: string;
	images?: string[];
};

export class PostFormatter {
	#imageUrlExtractor: ImageUrlExtractor;

	constructor(imageUrlExtractor: ImageUrlExtractor) {
		this.#imageUrlExtractor = imageUrlExtractor;
	}

	format(meta: PostMetaData, content: string) {
		const images = this.#imageUrlExtractor.extract(content);

		delete meta.images;

		if (images.length > 0) {
			meta.images = images;
		}

		return JSON.stringify(meta, undefined, 4) + '\n\n' + content;
	}
}
