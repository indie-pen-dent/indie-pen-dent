export class ImageUrlExtractor {
	#imageRegEx = /!\[.*?]\((.*?)\)/g;

	extract(content: string): string[] {
		const matches = content.matchAll(this.#imageRegEx);

		const imageUrls: string[] = [];

		for (const match of matches) {
			const imageUrl = match[1];
			imageUrls.push(imageUrl);
		}

		return imageUrls;
	}
}
