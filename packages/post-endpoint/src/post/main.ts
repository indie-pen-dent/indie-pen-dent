import {ImageUrlExtractor} from './image-url-extractor.js';
import {PostFormatter} from './post.js';

const imageUrlExtractor = new ImageUrlExtractor();

export const postFormatter = new PostFormatter(imageUrlExtractor);
